-- MySQL dump 10.13  Distrib 5.5.8, for Win32 (x86)
--
-- Host: localhost    Database: bd_Citas
-- ------------------------------------------------------
-- Server version	5.5.8-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `Id_Area` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Area` varchar(50) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Area`),
  UNIQUE KEY `nom_Area` (`nom_Area`),
  KEY `estadoarea` (`FKId_Estado`),
  CONSTRAINT `estadoarea` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Área de hospitalización',1),(2,'Policlínica',1),(3,'Quirófanos',1),(4,'Urgencias',1),(5,'Hospital de día',1);
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignac_inv`
--

DROP TABLE IF EXISTS `asignac_inv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignac_inv` (
  `Id_Asignac_inv` bigint(20) NOT NULL AUTO_INCREMENT,
  `cant_med` bigint(20) NOT NULL,
  `FKId_inventario_med` bigint(20) NOT NULL,
  `FKId_Medicamentos` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Asignac_inv`),
  KEY `asignacinv` (`FKId_inventario_med`),
  KEY `medasignac` (`FKId_Medicamentos`),
  CONSTRAINT `asignac_inv_ibfk_2` FOREIGN KEY (`FKId_Medicamentos`) REFERENCES `medicamentos` (`Id_Medicamentos`),
  CONSTRAINT `asignac_inv_ibfk_1` FOREIGN KEY (`FKId_inventario_med`) REFERENCES `inventario_med` (`Id_inventario_med`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignac_inv`
--

LOCK TABLES `asignac_inv` WRITE;
/*!40000 ALTER TABLE `asignac_inv` DISABLE KEYS */;
/*!40000 ALTER TABLE `asignac_inv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cita`
--

DROP TABLE IF EXISTS `cita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cita` (
  `Id_Cita` bigint(20) NOT NULL AUTO_INCREMENT,
  `FKId_Paciente` bigint(20) NOT NULL,
  `FKId_Medico` bigint(20) NOT NULL,
  `Fecha_Cita` date NOT NULL,
  `Nota` varchar(255) DEFAULT 'Sin nota',
  `Enfermedad` varchar(255) DEFAULT 'Sin Enfermedad',
  `Sintomas` varchar(255) DEFAULT 'Sin Sintomas',
  `Medicamentos` varchar(255) DEFAULT 'Sin Medicamentos',
  `FKId_Operario` bigint(20) NOT NULL,
  `FKId_Estado_Cita` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Cita`),
  KEY `pacientecita` (`FKId_Paciente`),
  KEY `medicocita` (`FKId_Medico`),
  KEY `operariocita` (`FKId_Operario`),
  KEY `estadocita` (`FKId_Estado_Cita`),
  CONSTRAINT `cita_ibfk_3` FOREIGN KEY (`FKId_Paciente`) REFERENCES `paciente` (`Id_Paciente`),
  CONSTRAINT `cita_ibfk_2` FOREIGN KEY (`FKId_Medico`) REFERENCES `medico` (`Id_Medico`),
  CONSTRAINT `estadocita` FOREIGN KEY (`FKId_Estado_Cita`) REFERENCES `estado_cita` (`Id_Estado_Cita`),
  CONSTRAINT `operariocita` FOREIGN KEY (`FKId_Operario`) REFERENCES `operario` (`Id_Operario`)
) ENGINE=InnoDB AUTO_INCREMENT=401 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cita`
--

LOCK TABLES `cita` WRITE;
/*!40000 ALTER TABLE `cita` DISABLE KEYS */;
INSERT INTO `cita` VALUES (1,2,31,'0000-00-00','lorem eu metus. In','Morbi','Donec nibh enim,','non',64,1),(2,27,22,'0000-00-00','Aenean gravida nunc sed pede.','pellentesque a,','Maecenas ornare egestas','arcu. Morbi',90,1),(3,74,96,'2015-08-08','malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in','tincidunt,','rutrum.','placerat eget,',86,4),(4,63,61,'2017-10-03','Aliquam vulputate','tellus non magna.','tellus.','tristique',50,2),(5,77,72,'0000-00-00','ornare sagittis felis. Donec tempor, est ac mattis semper,','Mauris non dui','Fusce','pede.',28,4),(6,99,90,'0000-00-00','sociis natoque penatibus et magnis dis parturient montes, nascetur','Donec','ornare,','Fusce fermentum',93,4),(7,99,65,'0000-00-00','a ultricies','at, libero. Morbi','dictum augue malesuada','arcu',99,1),(8,59,81,'0000-00-00','non magna. Nam','Aenean egestas hendrerit','ultricies','neque. Nullam',76,1),(9,53,16,'0000-00-00','nec mauris blandit mattis. Cras','Quisque','Donec nibh.','Praesent luctus.',83,3),(10,86,9,'0000-00-00','nec,','sit amet,','ipsum primis','aliquet lobortis,',20,4),(11,99,27,'0000-00-00','tristique pharetra. Quisque ac libero nec ligula consectetuer','augue','orci, consectetuer','enim non',2,2),(12,93,78,'0000-00-00','porttitor eros nec tellus. Nunc','ante bibendum','Donec feugiat metus','scelerisque mollis. Phasellus',15,3),(13,87,86,'0000-00-00','diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris.','est ac facilisis','nec','tristique pellentesque, tellus',55,2),(14,97,25,'2016-10-04','conubia nostra, per','Sed','id, blandit','lobortis ultrices. Vivamus',89,4),(15,74,82,'2016-06-11','Aliquam auctor, velit eget laoreet','sit amet','tortor','Cras sed leo.',20,2),(16,33,43,'0000-00-00','magna, malesuada vel, convallis in, cursus et, eros.','parturient','Mauris nulla. Integer','ac',27,2),(17,40,87,'0000-00-00','nec ante blandit','dapibus id,','Duis sit amet','Sed nunc est,',7,4),(18,73,27,'2015-03-11','et ultrices posuere cubilia Curae;','tincidunt pede','luctus','consequat',19,1),(19,89,45,'0000-00-00','nec ante. Maecenas','eu neque pellentesque','ultrices a, auctor','varius orci,',19,1),(20,20,25,'2015-01-07','egestas','sed','mauris, rhoncus','Ut',90,3),(21,83,74,'0000-00-00','quis massa. Mauris vestibulum, neque sed','molestie arcu.','enim. Curabitur','nec orci. Donec',26,2),(22,78,36,'2017-01-05','Cum sociis natoque','amet massa. Quisque','Maecenas iaculis','semper tellus',82,4),(23,53,50,'2016-04-04','orci.','feugiat tellus','Duis mi enim,','urna convallis erat,',54,2),(24,23,27,'2017-06-03','ipsum','risus. Donec','purus. Maecenas libero','sapien',46,3),(25,63,38,'0000-00-00','vulputate, nisi sem semper erat, in consectetuer','non dui','a, scelerisque','Suspendisse sagittis. Nullam',44,3),(26,19,65,'0000-00-00','In condimentum. Donec at arcu. Vestibulum ante ipsum','ornare, lectus ante','cursus a,','hendrerit. Donec porttitor',86,4),(27,86,13,'2016-05-07','ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac,','Phasellus','sagittis felis. Donec','arcu. Vestibulum',92,3),(28,99,80,'0000-00-00','massa non','facilisis, magna tellus','Sed neque. Sed','purus sapien, gravida',73,2),(29,43,19,'2017-04-04','ornare. Fusce mollis. Duis sit amet diam eu dolor','consectetuer adipiscing','Sed congue,','eleifend',16,2),(30,57,77,'0000-00-00','ipsum cursus vestibulum. Mauris magna. Duis','fermentum','ligula eu','non, sollicitudin',61,1),(31,16,90,'2017-05-05','consequat,','dolor dapibus','elit, a','mattis.',52,4),(32,48,20,'2017-05-05','luctus vulputate, nisi sem semper erat, in consectetuer','feugiat.','a, auctor','sit amet lorem',34,1),(33,77,72,'0000-00-00','lorem vitae odio sagittis semper. Nam tempor diam','Mauris','Vivamus','porttitor',78,4),(34,53,47,'0000-00-00','amet lorem semper auctor.','lobortis. Class','congue, elit','egestas,',44,1),(35,62,19,'2017-04-03','ornare lectus justo eu arcu. Morbi sit amet massa.','sit amet','Ut semper','Nullam lobortis quam',39,1),(36,6,47,'2015-11-10','pulvinar arcu et pede. Nunc','eleifend non,','tempus risus. Donec','scelerisque,',76,4),(37,81,15,'0000-00-00','sed tortor. Integer aliquam adipiscing lacus. Ut','elit','non,','sit amet, consectetuer',69,4),(38,59,14,'2017-06-04','nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus','montes, nascetur ridiculus','et tristique pellentesque,','Sed nulla',45,2),(39,88,5,'0000-00-00','velit. Quisque varius. Nam porttitor scelerisque','Suspendisse','lectus convallis est,','Sed',43,1),(40,97,8,'0000-00-00','Curabitur consequat, lectus sit','nisl','tempus','quam a felis',9,3),(41,97,57,'0000-00-00','turpis vitae purus gravida sagittis. Duis gravida. Praesent eu','cursus. Nunc mauris','Cum sociis','ipsum',87,3),(42,31,77,'0000-00-00','diam vel arcu. Curabitur ut','ut, nulla.','quam. Curabitur','mi enim, condimentum',17,1),(43,43,95,'0000-00-00','non justo. Proin non massa non ante','Nam tempor diam','facilisis eget, ipsum.','ultrices',56,1),(44,84,57,'2016-07-01','ut odio vel est tempor bibendum.','nunc ac mattis','Ut','ultricies',17,2),(45,34,75,'0000-00-00','elementum purus,','Nulla semper','non','in, tempus eu,',59,4),(46,63,27,'0000-00-00','dolor sit','eget laoreet','egestas rhoncus. Proin','Suspendisse sed dolor.',10,3),(47,12,87,'0000-00-00','nisi sem semper erat, in consectetuer ipsum nunc id enim.','purus,','Donec','sociis natoque',37,3),(48,46,62,'2015-05-07','mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus','Duis risus','eu','odio, auctor vitae,',44,4),(49,19,80,'2015-12-09','ipsum','turpis. Nulla','est. Nunc ullamcorper,','quis',9,2),(50,13,2,'2016-12-08','Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis','dolor sit amet,','Ut sagittis','pellentesque a,',23,3),(51,46,21,'0000-00-00','Donec tincidunt. Donec vitae erat vel pede','orci luctus','et, lacinia vitae,','tincidunt',25,1),(52,86,18,'2015-06-08','rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus','Sed molestie. Sed','eget tincidunt dui','arcu',72,1),(53,46,74,'0000-00-00','diam dictum sapien. Aenean massa. Integer vitae nibh.','et','Ut nec urna','habitant morbi',98,3),(54,83,10,'0000-00-00','magna a tortor. Nunc commodo auctor velit. Aliquam nisl.','ipsum','fermentum','Proin vel arcu',66,3),(55,73,43,'2016-12-12','non leo. Vivamus nibh dolor, nonummy ac, feugiat non,','volutpat','ipsum','vel,',4,4),(56,94,25,'0000-00-00','Fusce dolor quam, elementum at, egestas','egestas','pharetra,','et risus.',10,1),(57,41,64,'2016-07-08','Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas','penatibus et','scelerisque mollis.','ut, molestie in,',88,4),(58,29,57,'2016-02-05','velit justo nec ante. Maecenas mi felis, adipiscing fringilla,','non nisi. Aenean','massa. Suspendisse','urna suscipit',85,3),(59,78,92,'2017-03-05','mattis ornare, lectus','scelerisque','ut quam','convallis in, cursus',95,4),(60,20,50,'2015-10-09','id nunc interdum feugiat. Sed nec metus facilisis','primis in','Suspendisse non leo.','eget massa.',8,3),(61,82,48,'2017-01-04','adipiscing, enim mi','odio. Aliquam vulputate','Phasellus','orci, consectetuer',100,3),(62,80,1,'0000-00-00','a, enim. Suspendisse','nec, malesuada','ultrices sit','Donec est mauris,',70,2),(63,45,78,'0000-00-00','pede. Cum sociis natoque penatibus et magnis dis parturient montes,','vulputate velit eu','gravida nunc sed','a nunc. In',21,4),(64,37,8,'2016-12-03','Donec elementum, lorem ut aliquam iaculis, lacus pede','pulvinar arcu','torquent per','euismod et,',57,3),(65,98,67,'2017-01-02','vel, venenatis','ornare,','lorem, sit amet','eu tellus.',64,2),(66,42,94,'0000-00-00','Curabitur','Aenean','blandit','ut',42,3),(67,80,95,'2017-06-02','venenatis a, magna. Lorem ipsum dolor sit','magna a','ligula. Aenean','cursus. Nunc',91,1),(68,31,65,'0000-00-00','venenatis a, magna. Lorem ipsum dolor','sed dictum','aliquet. Phasellus','malesuada vel,',16,4),(69,81,63,'0000-00-00','arcu ac orci. Ut semper pretium neque. Morbi','ornare. Fusce mollis.','tincidunt','molestie dapibus ligula.',39,4),(70,26,26,'2017-04-03','lobortis.','enim.','natoque','molestie dapibus ligula.',57,3),(71,79,50,'0000-00-00','In lorem. Donec elementum,','metus facilisis','semper','pharetra sed, hendrerit',92,4),(72,98,3,'0000-00-00','euismod in, dolor. Fusce feugiat. Lorem ipsum','euismod','volutpat nunc','nascetur ridiculus mus.',87,3),(73,80,48,'0000-00-00','a mi fringilla mi lacinia','risus. Nunc','sociis natoque','pede sagittis',76,1),(74,59,11,'0000-00-00','tincidunt nibh.','interdum','leo.','lacus.',51,4),(75,82,66,'0000-00-00','augue','convallis, ante','nisl.','dapibus',50,1),(76,13,41,'0000-00-00','consectetuer euismod','sit amet nulla.','tortor. Nunc commodo','parturient',48,3),(77,24,81,'0000-00-00','Maecenas libero est, congue a, aliquet vel, vulputate eu, odio.','tortor nibh','Quisque','Etiam',98,4),(78,83,6,'0000-00-00','Donec nibh. Quisque','orci. Donec nibh.','lectus','nunc risus varius',32,2),(79,63,38,'2016-06-07','posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel','Proin mi. Aliquam','mauris ut','semper',13,3),(80,7,9,'2016-08-12','ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor','pellentesque eget,','malesuada','morbi',37,3),(81,78,22,'0000-00-00','massa. Suspendisse eleifend. Cras','arcu. Vestibulum','neque','elit.',13,2),(82,58,83,'2016-09-05','auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare','vehicula','Donec','erat eget ipsum.',50,4),(83,2,16,'0000-00-00','nascetur','non, dapibus rutrum,','nec','a feugiat',54,2),(84,63,14,'0000-00-00','Ut semper pretium neque. Morbi quis','lacinia.','nec urna','semper,',21,3),(85,50,25,'0000-00-00','velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem','eget magna. Suspendisse','erat eget ipsum.','inceptos hymenaeos.',58,3),(86,10,51,'2015-05-07','leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod in,','nulla. Integer vulputate,','Vivamus euismod','sollicitudin orci',98,1),(87,66,74,'2016-03-01','ut eros','facilisis','Phasellus','Praesent interdum ligula',43,3),(88,90,37,'0000-00-00','Donec dignissim magna a tortor.','metus','lorem eu','condimentum eget,',85,4),(89,92,100,'0000-00-00','magnis dis parturient montes, nascetur ridiculus mus. Proin','enim, condimentum eget,','eget','et',54,4),(90,68,21,'0000-00-00','imperdiet ullamcorper. Duis at lacus. Quisque','amet, consectetuer adipiscing','tellus eu augue','vulputate velit',21,2),(91,42,91,'0000-00-00','faucibus lectus, a sollicitudin orci','magna a neque.','et ultrices','natoque penatibus',68,3),(92,33,89,'2016-12-12','libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus','mi','nibh','lectus. Cum sociis',47,2),(93,98,5,'2015-06-08','Integer vulputate,','rutrum eu, ultrices','lorem semper','sit',48,3),(94,88,53,'2016-04-02','tempor bibendum. Donec','cursus, diam','Integer id magna','Nulla',67,4),(95,19,99,'2017-08-01','hendrerit consectetuer, cursus et, magna. Praesent interdum','at','ornare','Morbi accumsan laoreet',19,3),(96,75,51,'0000-00-00','amet, consectetuer adipiscing elit.','lorem ac','neque','mollis. Integer',92,3),(97,83,83,'0000-00-00','tempus eu, ligula. Aenean euismod mauris eu elit.','Vivamus','ipsum sodales','dolor.',67,2),(98,70,30,'2016-08-11','facilisis lorem tristique aliquet. Phasellus fermentum convallis','euismod urna. Nullam','Integer tincidunt aliquam','mollis nec, cursus',27,2),(99,68,22,'2017-09-01','sociis natoque penatibus et','a,','cursus','erat',87,3),(100,87,5,'2015-09-12','consectetuer adipiscing elit. Aliquam','ante','dictum','Morbi neque',98,3);
/*!40000 ALTER TABLE `cita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `citasactivasporpaciente`
--

DROP TABLE IF EXISTS `citasactivasporpaciente`;
/*!50001 DROP VIEW IF EXISTS `citasactivasporpaciente`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `citasactivasporpaciente` (
  `Id_Cita` bigint(20),
  `nom_Paciente` varchar(50),
  `Fecha_Cita` date,
  `nom_Estado_Cita` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `citaspendientes`
--

DROP TABLE IF EXISTS `citaspendientes`;
/*!50001 DROP VIEW IF EXISTS `citaspendientes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `citaspendientes` (
  `Id_Cita` bigint(20),
  `Fecha_Cita` date,
  `nom_Estado_Cita` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `citaspendientesporpaciente`
--

DROP TABLE IF EXISTS `citaspendientesporpaciente`;
/*!50001 DROP VIEW IF EXISTS `citaspendientesporpaciente`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `citaspendientesporpaciente` (
  `Id_Cita` bigint(20),
  `nom_Paciente` varchar(50),
  `Fecha_Cita` date,
  `nom_Estado_Cita` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `citaspendpacmed`
--

DROP TABLE IF EXISTS `citaspendpacmed`;
/*!50001 DROP VIEW IF EXISTS `citaspendpacmed`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `citaspendpacmed` (
  `Id_Cita` bigint(20),
  `nom_Paciente` varchar(50),
  `nom_Medico` varchar(50),
  `Fecha_Cita` date,
  `nom_Estado_Cita` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `citaspendpacmedoper`
--

DROP TABLE IF EXISTS `citaspendpacmedoper`;
/*!50001 DROP VIEW IF EXISTS `citaspendpacmedoper`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `citaspendpacmedoper` (
  `Id_Cita` bigint(20),
  `nom_Paciente` varchar(50),
  `nom_Medico` varchar(50),
  `nom_Oper` varchar(50),
  `Fecha_Cita` date,
  `nom_Estado_Cita` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `consultorio`
--

DROP TABLE IF EXISTS `consultorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consultorio` (
  `Id_Consultorio` bigint(20) NOT NULL AUTO_INCREMENT,
  `num_Cons` bigint(20) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Consultorio`),
  KEY `estadocons` (`FKId_Estado`),
  CONSTRAINT `estadocons` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consultorio`
--

LOCK TABLES `consultorio` WRITE;
/*!40000 ALTER TABLE `consultorio` DISABLE KEYS */;
/*!40000 ALTER TABLE `consultorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_diag_med`
--

DROP TABLE IF EXISTS `det_diag_med`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_diag_med` (
  `Id_det_diag_med` bigint(20) NOT NULL AUTO_INCREMENT,
  `cant_medicamento` bigint(20) NOT NULL,
  `precio_tot` bigint(20) NOT NULL,
  `FKId_Diagnostico` bigint(20) NOT NULL,
  `FKId_Medicamentos` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_det_diag_med`),
  KEY `detdiagmed` (`FKId_Diagnostico`),
  KEY `detmeddiag` (`FKId_Medicamentos`),
  CONSTRAINT `detdiagmed` FOREIGN KEY (`FKId_Diagnostico`) REFERENCES `diagnostico` (`Id_Diagnostico`),
  CONSTRAINT `detmeddiag` FOREIGN KEY (`FKId_Medicamentos`) REFERENCES `medicamentos` (`Id_Medicamentos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_diag_med`
--

LOCK TABLES `det_diag_med` WRITE;
/*!40000 ALTER TABLE `det_diag_med` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_diag_med` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnostico`
--

DROP TABLE IF EXISTS `diagnostico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnostico` (
  `Id_Diagnostico` bigint(20) NOT NULL AUTO_INCREMENT,
  `desc_Diag` varchar(500) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  `FKId_Historia_Clinica` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Diagnostico`),
  KEY `estadodiag` (`FKId_Estado`),
  KEY `diaghistoria` (`FKId_Historia_Clinica`),
  CONSTRAINT `diagnostico_ibfk_1` FOREIGN KEY (`FKId_Historia_Clinica`) REFERENCES `historia_clinica` (`Id_Historia_Clinica`),
  CONSTRAINT `estadodiag` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnostico`
--

LOCK TABLES `diagnostico` WRITE;
/*!40000 ALTER TABLE `diagnostico` DISABLE KEYS */;
/*!40000 ALTER TABLE `diagnostico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `Id_EPS` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_EPS` varchar(50) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_EPS`),
  UNIQUE KEY `nom_EPS` (`nom_EPS`),
  KEY `estadoeps` (`FKId_Estado`),
  CONSTRAINT `eps_ibfk_1` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
INSERT INTO `eps` VALUES (1,'Sanitas',1),(2,'Caprecom',1),(3,'Salud Total',1),(4,'Capital Salud',1),(5,'Cafe Salud',1);
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especialidad`
--

DROP TABLE IF EXISTS `especialidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `especialidad` (
  `Id_Especialidad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Esp` varchar(50) NOT NULL,
  `desc_Esp` varchar(255) DEFAULT 'Sin Descripción',
  `FKId_Estado` bigint(20) NOT NULL,
  `FKId_Tipo_Espec` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Especialidad`),
  KEY `estadoespec` (`FKId_Estado`),
  KEY `tipoespecialidad` (`FKId_Tipo_Espec`),
  CONSTRAINT `especialidad_ibfk_1` FOREIGN KEY (`FKId_Tipo_Espec`) REFERENCES `tipo_especialidad` (`Id_Tipo_Espec`),
  CONSTRAINT `estadoespec` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especialidad`
--

LOCK TABLES `especialidad` WRITE;
/*!40000 ALTER TABLE `especialidad` DISABLE KEYS */;
INSERT INTO `especialidad` VALUES (6,'pediatría','Sin Descripción',1,1),(7,'Alergología','Sin Descripción',1,1),(8,'Cirugía cardiovascular','Sin Descripción',1,3),(9,'Angiología y cirugía vascular','Sin Descripción',1,2),(10,'Análisis clínicos','Sin Descripción',1,4);
/*!40000 ALTER TABLE `especialidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `Id_Estado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Estado` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Estado`),
  UNIQUE KEY `nom_Estado` (`nom_Estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Activo'),(2,'Inactivo');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_cita`
--

DROP TABLE IF EXISTS `estado_cita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_cita` (
  `Id_Estado_Cita` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Estado_Cita` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Estado_Cita`),
  UNIQUE KEY `nom_Estado_Cita` (`nom_Estado_Cita`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_cita`
--

LOCK TABLES `estado_cita` WRITE;
/*!40000 ALTER TABLE `estado_cita` DISABLE KEYS */;
INSERT INTO `estado_cita` VALUES (4,'Cancelada'),(1,'Finalizada'),(3,'No Asistió'),(2,'Pendiente');
/*!40000 ALTER TABLE `estado_cita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `Id_Genero` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Genero` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Genero`),
  UNIQUE KEY `nom_Genero` (`nom_Genero`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` VALUES (2,'Femenino'),(1,'Masculino');
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historia_clinica`
--

DROP TABLE IF EXISTS `historia_clinica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historia_clinica` (
  `Id_Historia_Clinica` bigint(20) NOT NULL AUTO_INCREMENT,
  `Fecha_creacion` date NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  `FKId_Paciente` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Historia_Clinica`),
  KEY `estadohistoriaclinica` (`FKId_Estado`),
  KEY `historiapaciente` (`FKId_Paciente`),
  CONSTRAINT `historia_clinica_ibfk_2` FOREIGN KEY (`FKId_Paciente`) REFERENCES `paciente` (`Id_Paciente`),
  CONSTRAINT `historia_clinica_ibfk_1` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historia_clinica`
--

LOCK TABLES `historia_clinica` WRITE;
/*!40000 ALTER TABLE `historia_clinica` DISABLE KEYS */;
/*!40000 ALTER TABLE `historia_clinica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario` (
  `Id_Horario` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Hor` varchar(50) NOT NULL,
  `in_hor` time NOT NULL,
  `out_hor` time NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Horario`),
  UNIQUE KEY `nom_Hor` (`nom_Hor`),
  KEY `estadohorario` (`FKId_Estado`),
  CONSTRAINT `horario_ibfk_1` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario`
--

LOCK TABLES `horario` WRITE;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
INSERT INTO `horario` VALUES (1,'Mañana','00:00:06','00:00:14',1),(2,'Tarde','00:00:14','00:00:22',1),(3,'Noche','00:00:22','00:00:06',1);
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventario_med`
--

DROP TABLE IF EXISTS `inventario_med`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventario_med` (
  `Id_inventario_med` bigint(20) NOT NULL AUTO_INCREMENT,
  `Stock_in` bigint(20) NOT NULL,
  `Stock_out` bigint(20) NOT NULL,
  `Stock_real` bigint(20) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_inventario_med`),
  KEY `estadoinv` (`FKId_Estado`),
  CONSTRAINT `inventario_med_ibfk_1` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventario_med`
--

LOCK TABLES `inventario_med` WRITE;
/*!40000 ALTER TABLE `inventario_med` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventario_med` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicamentos`
--

DROP TABLE IF EXISTS `medicamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicamentos` (
  `Id_Medicamentos` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Medicamento` varchar(50) NOT NULL,
  `desc_Medicamento` varchar(200) DEFAULT 'Sin Descripción',
  `precio` bigint(20) NOT NULL,
  `FKId_Tipo_Medicamento` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Medicamentos`),
  UNIQUE KEY `nom_Medicamento` (`nom_Medicamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicamentos`
--

LOCK TABLES `medicamentos` WRITE;
/*!40000 ALTER TABLE `medicamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `medicamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medico`
--

DROP TABLE IF EXISTS `medico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medico` (
  `Id_Medico` bigint(20) NOT NULL AUTO_INCREMENT,
  `FKId_Area` bigint(20) NOT NULL,
  `FKId_Especialidad` bigint(20) NOT NULL,
  `FKId_Tipo_Doc` bigint(20) NOT NULL,
  `num_doc_Med` bigint(20) NOT NULL,
  `nom_Medico` varchar(50) NOT NULL,
  `apell_Medico` varchar(50) NOT NULL,
  `FKId_Genero` bigint(20) NOT NULL,
  `fecha_nac_Med` date NOT NULL,
  `direc_Med` varchar(255) DEFAULT 'Sin dirección',
  `email_Med` varchar(255) DEFAULT 'Sin Email',
  `telf_fijo_Med` bigint(20) NOT NULL,
  `telf_cel_Med` bigint(20) NOT NULL,
  `FKId_Horario` bigint(20) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Medico`),
  KEY `areamedico` (`FKId_Area`),
  KEY `especmedico` (`FKId_Especialidad`),
  KEY `tipodocmedico` (`FKId_Tipo_Doc`),
  KEY `generomedico` (`FKId_Genero`),
  KEY `horariomedico` (`FKId_Horario`),
  KEY `estadomedico` (`FKId_Estado`),
  CONSTRAINT `areamedico` FOREIGN KEY (`FKId_Area`) REFERENCES `area` (`Id_Area`),
  CONSTRAINT `especmedico` FOREIGN KEY (`FKId_Especialidad`) REFERENCES `especialidad` (`Id_Especialidad`),
  CONSTRAINT `estadomedico` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`),
  CONSTRAINT `generomedico` FOREIGN KEY (`FKId_Genero`) REFERENCES `genero` (`Id_Genero`),
  CONSTRAINT `horariomedico` FOREIGN KEY (`FKId_Horario`) REFERENCES `horario` (`Id_Horario`),
  CONSTRAINT `tipodocmedico` FOREIGN KEY (`FKId_Tipo_Doc`) REFERENCES `tipo_documento` (`Id_Tipo_Doc`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medico`
--

LOCK TABLES `medico` WRITE;
/*!40000 ALTER TABLE `medico` DISABLE KEYS */;
INSERT INTO `medico` VALUES (1,3,9,3,5810788859,'Kylan','Dale',2,'0000-00-00','Ap #989-8488 Enim Ave','mauris@orcilacusvestibulum.org',8311671185,8885016808,2,2),(2,1,9,1,7245561716,'Kane','Rodgers',2,'0000-00-00','1044 Magna Ave','varius@consequatauctornunc.co.uk',4070044375,6914711041,2,2),(3,4,10,3,6077236442,'Brenna','Conrad',1,'0000-00-00','P.O. Box 298, 2223 Vel Road','vel@pede.ca',6151903319,4122239906,2,2),(4,4,8,2,4478763915,'Keegan','Leon',2,'0000-00-00','Ap #820-8496 Tellus. Rd.','fermentum.vel.mauris@odio.edu',8073828888,6963972150,3,1),(5,4,7,2,6615486629,'Lars','Pitts',1,'0000-00-00','3981 Quis Road','Nunc@aliquetmagna.org',6938354842,8735064838,2,1),(6,3,10,3,4406713524,'Hermione','Fry',1,'0000-00-00','517-5755 Ut Av.','Etiam@lectusantedictum.com',4826458444,5057076007,3,2),(7,1,10,3,4660746121,'Carlos','Mays',2,'0000-00-00','Ap #516-3173 Urna St.','lectus@aliquetPhasellusfermentum.org',5062565315,8198595048,2,2),(8,4,8,1,4059002103,'Jessamine','Jimenez',1,'0000-00-00','Ap #124-5320 Blandit. Av.','ac@lobortisrisusIn.net',7172886162,5909242517,1,2),(9,3,10,3,4292824881,'Dolan','Ford',1,'0000-00-00','P.O. Box 413, 8392 Dui. Rd.','est.congue@nulla.com',8915851406,8266787097,3,2),(10,2,9,1,5393636323,'Francis','Hamilton',2,'0000-00-00','Ap #727-3817 Faucibus Street','aliquam.arcu.Aliquam@nisl.com',6530420912,7304607398,1,2),(11,3,9,3,5954750119,'Lillith','Aguirre',1,'0000-00-00','Ap #111-4566 Suspendisse St.','arcu.Vestibulum.ante@rhoncusNullam.edu',4815148246,5628433687,2,2),(12,3,8,1,6663585330,'Courtney','Cobb',2,'0000-00-00','P.O. Box 501, 1313 Enim, Avenue','orci.consectetuer.euismod@Nullamutnisi.ca',6840635502,5209375763,2,2),(13,4,10,3,5789196009,'Nora','Zamora',2,'0000-00-00','529-9516 Felis Avenue','blandit@sitamet.org',4557283070,7761547634,3,2),(14,1,8,2,7698412853,'Alec','Vang',1,'0000-00-00','Ap #542-7163 Vel Ave','tincidunt.orci@infelisNulla.co.uk',8050118006,5529738041,1,2),(15,1,8,1,5498878686,'Venus','Moran',1,'0000-00-00','3987 In Street','nibh.Aliquam.ornare@Morbinequetellus.edu',6184375918,4957701301,2,2),(16,1,7,3,4784280296,'Autumn','Boyd',2,'0000-00-00','4967 Ligula Rd.','ornare.lectus.ante@loremacrisus.ca',7442491777,6197955744,1,2),(17,4,7,3,5468017960,'Wallace','Roy',2,'0000-00-00','P.O. Box 498, 1471 Non, Rd.','lorem.lorem.luctus@Suspendisseacmetus.org',5919778909,4862960575,1,1),(18,2,7,3,5828747367,'Chase','Herrera',1,'0000-00-00','4552 Lacus. St.','vitae.posuere.at@at.co.uk',7034939710,7257298349,2,2),(19,1,10,2,5401629289,'Harriet','Boyd',1,'0000-00-00','5064 Auctor, Av.','erat@erosProin.org',6516525642,8740856609,2,1),(20,2,8,3,5092273978,'Erasmus','Kidd',2,'0000-00-00','127-7494 Elementum, Street','risus.Quisque.libero@DonecestNunc.edu',4433144522,4265688824,2,2),(21,1,6,2,6530686092,'Ina','Walker',2,'0000-00-00','526-1866 Mattis. Ave','sociis@commodoat.co.uk',4223803732,8022117809,3,1),(22,4,10,2,4868043747,'Ignacia','Salinas',2,'0000-00-00','Ap #873-8261 Felis. Street','eleifend.non.dapibus@sociosquadlitora.com',7712224232,5740363787,1,1),(23,3,9,2,5909306137,'Noah','Terry',1,'0000-00-00','1100 Lorem Rd.','Vestibulum.accumsan@risusInmi.org',7542382966,5225109652,3,1),(24,2,10,2,5105913108,'Fredericka','Bradshaw',2,'0000-00-00','553-210 Nunc Ave','rhoncus@Etiam.com',8036775460,6865682857,1,2),(25,2,10,2,6102092346,'Nathan','Garner',2,'0000-00-00','Ap #111-1579 In St.','at.sem.molestie@luctussitamet.co.uk',4364015169,5616423975,1,2),(26,2,10,1,4018103061,'Christen','Fitzgerald',1,'0000-00-00','165-1128 Mauris St.','molestie@ac.com',7437548762,7948841614,3,2),(27,1,9,3,6930583464,'Ifeoma','Parks',2,'0000-00-00','1704 Neque Ave','mattis.ornare@eget.co.uk',4816882949,6431959312,3,1),(28,3,8,1,4483262932,'Tyrone','Davis',2,'0000-00-00','Ap #950-8007 Ut Road','cursus.purus@mattis.org',5178636674,7219476144,2,2),(29,2,10,2,7296520682,'Jocelyn','Baldwin',1,'0000-00-00','5856 Egestas. Av.','sollicitudin.orci.sem@ac.co.uk',8230304512,8804792165,2,1),(30,3,10,3,4612573044,'Deacon','Lowery',1,'0000-00-00','P.O. Box 934, 4977 Adipiscing Av.','felis.purus.ac@Nullafacilisis.edu',4829470982,6712507143,1,1),(31,2,8,2,7928203134,'Nissim','Holder',2,'0000-00-00','7566 Nulla St.','pellentesque.Sed@fringilla.ca',8871455790,7976976839,2,1),(32,3,7,2,4590791294,'Madonna','Gould',2,'0000-00-00','Ap #755-591 Nec St.','magna@tortor.com',5431348028,6135631700,1,2),(33,3,7,2,5568504291,'Nathan','Brewer',1,'0000-00-00','534-6785 In Av.','cursus.in.hendrerit@Phasellusdapibus.edu',8314485479,7819143491,2,2),(34,4,8,2,5916222556,'Constance','Baxter',2,'0000-00-00','202-4851 Sem Rd.','odio@CraspellentesqueSed.org',5500554611,4123082154,1,2),(35,1,8,1,5718797907,'Melodie','Gillespie',1,'0000-00-00','Ap #835-4078 Dolor Rd.','Ut.nec@arcuNuncmauris.net',5470986767,5357239454,2,1),(36,2,8,1,6220512092,'Iola','Neal',1,'0000-00-00','P.O. Box 249, 9196 Dictum Av.','orci@ac.ca',7023734130,5623060498,2,2),(37,3,10,3,6127706992,'Roth','York',1,'0000-00-00','776-2130 Eu Ave','eget@Ut.co.uk',4598823153,6692591865,1,1),(38,3,9,3,6343023893,'Myra','Roy',1,'0000-00-00','3122 Aliquet Rd.','lectus@felis.edu',5576573313,8630219669,2,2),(39,2,6,2,5325980773,'Adara','Ashley',1,'0000-00-00','P.O. Box 896, 8622 Cursus Street','nunc.Quisque@etlibero.com',5874126247,8127510274,3,1),(40,3,7,3,5924381891,'Nathan','Pena',1,'0000-00-00','2823 Purus Ave','nostra@Integervitae.ca',4637100576,5892391844,1,2),(41,3,7,1,6351501938,'Quyn','Bonner',1,'0000-00-00','854-8011 Blandit St.','sodales.at@convalliserateget.net',4116099561,4905853794,1,1),(42,3,9,1,7721451646,'Chaney','Hawkins',1,'0000-00-00','P.O. Box 510, 865 Mauris Av.','quis.accumsan@commodo.net',7249986414,7978714841,2,1),(43,1,7,2,4444565368,'Silas','Ward',2,'0000-00-00','283-3521 Non, St.','sed.consequat.auctor@sempertellus.org',6256345607,6454655482,3,1),(44,3,8,3,7277430555,'Gillian','Harper',2,'0000-00-00','Ap #420-588 Donec Avenue','mauris@massaQuisqueporttitor.net',6075745794,5033898806,3,2),(45,3,6,2,7751716501,'Jolene','Meyers',2,'0000-00-00','248-3568 Suscipit, Road','vel@turpis.net',8941324853,4476541179,2,2),(46,4,6,2,5853039969,'Kylee','Kane',2,'0000-00-00','920-5076 Per Road','netus.et@dapibusquam.com',8825799487,7398237385,1,2),(47,4,6,3,6359501943,'Raya','Small',2,'0000-00-00','Ap #633-6198 Luctus Av.','ultricies@esttempor.com',8464721866,8082148997,2,1),(48,3,10,2,4949507560,'Rafael','Hyde',2,'0000-00-00','Ap #196-823 Ligula. Street','tortor.nibh.sit@interdumfeugiatSed.net',7251059344,4688493962,1,1),(49,3,7,3,4605778075,'Keefe','Boyle',2,'0000-00-00','P.O. Box 719, 1400 In, Avenue','et.magnis.dis@ante.org',5503708290,5530693406,2,1),(50,3,7,1,7347917987,'Sylvester','Raymond',2,'0000-00-00','P.O. Box 814, 6582 Consequat St.','posuere.cubilia@torquentper.net',8088629964,4929607162,3,1),(51,3,8,3,6826128323,'Abraham','Soto',2,'0000-00-00','P.O. Box 885, 7080 Ac, Rd.','ac.facilisis.facilisis@estac.com',7112106549,8976907773,1,1),(52,3,6,3,6175997894,'Lamar','Waller',1,'0000-00-00','837-5734 Parturient Road','sit.amet.risus@sitametnulla.net',6538551851,5280791147,2,2),(53,4,8,1,4678613973,'Orson','Santana',2,'0000-00-00','Ap #145-7988 Pellentesque Avenue','augue.scelerisque.mollis@purus.org',8789217557,8689453284,2,1),(54,1,10,2,4465312318,'Colette','Pollard',2,'0000-00-00','768 Sed Rd.','mi.pede@aliquam.ca',6506112887,5637243400,1,1),(55,3,8,1,5834507354,'Hamish','Blanchard',2,'0000-00-00','910-8601 Non Road','adipiscing.lobortis@duiCum.ca',5160010616,7258252936,2,1),(56,1,8,3,6696295317,'Beau','Hood',2,'0000-00-00','Ap #182-5685 Sed Street','eleifend.nunc@at.com',4264393081,5476126271,3,1),(57,4,7,1,5260096747,'Lenore','Hendrix',1,'0000-00-00','1459 Dapibus Ave','accumsan.neque@nonummyutmolestie.edu',8354092398,5727740054,1,1),(58,4,7,3,6229497865,'Evangeline','Webster',1,'0000-00-00','3728 Mauris Rd.','felis.purus.ac@tellusAeneanegestas.ca',8975032220,6205534524,1,1),(59,3,7,2,4747937908,'Naida','Armstrong',2,'0000-00-00','Ap #991-9566 Nulla Avenue','ac@vitaediamProin.ca',5434907808,7000973649,2,2),(60,1,9,2,4415780903,'Jade','Newton',2,'0000-00-00','7612 Vivamus Ave','sagittis@eu.net',8875108060,5688327065,2,1),(61,1,6,3,5132805923,'Gretchen','Gates',2,'0000-00-00','Ap #127-8877 A, Road','ligula.Aenean@Sedetlibero.org',6449590606,8175797627,3,2),(62,4,9,3,5080299673,'Willow','Gilmore',2,'0000-00-00','4189 Non Ave','justo.nec@Proinnislsem.org',7559589409,8634022923,2,1),(63,4,10,3,4459816616,'Cole','Martinez',2,'0000-00-00','5897 Quis, Street','est.Mauris@consectetueripsum.ca',5833894995,8725549869,2,1),(64,2,6,3,5189025685,'Allegra','Battle',2,'0000-00-00','Ap #755-9667 Vehicula Av.','non.magna.Nam@mi.org',5415353925,7072802115,3,2),(65,1,9,1,6261444885,'Sigourney','Nixon',2,'0000-00-00','203-4664 Et Avenue','tincidunt@auctorodioa.org',5721758193,7352763299,3,2),(66,2,6,3,4699025845,'Marcia','Calhoun',1,'0000-00-00','7517 Phasellus Street','massa@pellentesquea.edu',5790469480,8471506386,2,1),(67,1,7,3,7446788151,'Odysseus','Conrad',2,'0000-00-00','2506 Amet, Road','eget@ultricesposuere.ca',8514445139,6856974313,3,1),(68,1,6,2,4464785356,'Plato','Guerrero',1,'0000-00-00','P.O. Box 353, 9125 At Avenue','Duis.elementum@justoPraesentluctus.net',4364742945,7723801342,1,2),(69,2,6,1,5143676603,'Abraham','Ellison',1,'0000-00-00','118-685 Vitae, Rd.','magna@Sednulla.com',6501784023,4687830243,1,2),(70,2,9,1,4389791730,'Oscar','Walters',1,'0000-00-00','4243 Molestie Street','commodo.ipsum.Suspendisse@nunc.ca',4306190317,6939150906,3,2),(71,4,10,1,4146490914,'Russell','Delacruz',2,'0000-00-00','2062 Tempor Avenue','dui.in@arcu.org',8102720192,6824686430,3,2),(72,4,10,1,7979992768,'Lani','Walter',2,'0000-00-00','Ap #419-6449 Montes, Ave','aliquet.Phasellus@utquam.com',8709769555,7918778117,1,1),(73,3,7,3,7458384615,'Mara','Velasquez',1,'0000-00-00','795-8902 Sed Rd.','dignissim@nequenon.net',6293766665,4299326740,3,1),(74,4,10,2,4092130081,'Garrett','Mendez',2,'0000-00-00','1543 Arcu. Av.','a.aliquet.vel@In.ca',4819429496,6803557536,3,1),(75,2,9,3,6395734454,'Dominic','Fitzgerald',1,'0000-00-00','3917 Nulla. Ave','dui.in.sodales@conubianostraper.co.uk',6542043589,4176865798,1,2),(76,1,6,1,4172395454,'Denise','Holman',2,'0000-00-00','789-9385 Lorem St.','massa.Suspendisse@liberolacus.ca',7980532582,4753829437,3,1),(77,2,10,2,5880732877,'Dominique','Meyer',1,'0000-00-00','P.O. Box 982, 9338 Aliquam Street','Pellentesque@sem.edu',5158574668,4394586580,3,2),(78,3,6,1,7295102656,'Bianca','Britt',1,'0000-00-00','8892 Quam. Avenue','non@nibh.ca',5518466109,8759453217,3,1),(79,1,7,3,6865944339,'Olivia','Harrell',1,'0000-00-00','P.O. Box 667, 1856 Nunc Road','nulla@Inat.com',5339969041,7786626507,3,2),(80,3,6,3,7335975642,'Michael','Perez',1,'0000-00-00','625-9559 Eu Avenue','egestas@luctussit.edu',6102158430,7920340352,2,2),(81,2,7,3,5130577691,'Brian','Walter',2,'0000-00-00','P.O. Box 872, 8317 A Street','nec@Quisqueac.co.uk',5158861997,7421568074,1,2),(82,4,9,3,5991878532,'Serina','Newton',1,'0000-00-00','6296 At, Ave','mi@fermentumconvallisligula.co.uk',5938498791,5120395022,2,1),(83,4,7,1,5876438165,'Xavier','Myers',1,'0000-00-00','P.O. Box 992, 371 Netus Rd.','egestas.Sed.pharetra@cursuset.net',4736991427,7922743257,1,1),(84,3,10,2,5020342627,'Grady','Webster',2,'0000-00-00','P.O. Box 504, 1132 Cras Ave','nisi.Mauris@nonegestasa.com',8836676578,5636325929,2,1),(85,4,8,2,5575257281,'Celeste','Neal',2,'0000-00-00','P.O. Box 493, 7800 Gravida Rd.','consequat.nec.mollis@dolorDonec.ca',6097349563,6467042287,2,1),(86,4,7,2,7706522959,'Amela','Sloan',2,'0000-00-00','P.O. Box 706, 8381 Fermentum St.','tellus.faucibus@Praesent.ca',6486006538,5940476112,3,1),(87,4,10,2,5431772230,'Anjolie','Gentry',2,'0000-00-00','Ap #732-7123 Nibh. Ave','in@Utsagittislobortis.co.uk',8208125002,8977667879,3,1),(88,4,8,1,5836993208,'Shelly','Cooper',2,'0000-00-00','4292 Ut, Rd.','imperdiet.non.vestibulum@urnaNullamlobortis.edu',4009655563,7431149258,3,2),(89,2,10,2,5337561806,'Christine','Weber',2,'0000-00-00','8135 Enim Av.','euismod@commodoauctor.edu',4231060804,7240151105,2,2),(90,1,9,1,6638413239,'Marah','Russo',1,'0000-00-00','891 Volutpat Rd.','nec.leo@noncursus.com',6523565465,7245027911,2,1),(91,4,8,3,7720118618,'Lareina','Short',2,'0000-00-00','4101 Tempus St.','pede@imperdiet.org',7896790759,7076817924,3,1),(92,3,9,3,5831230847,'Erica','Mayer',2,'0000-00-00','5896 Odio. St.','feugiat@felisegetvarius.com',7895246515,8289108988,2,2),(93,3,9,2,7286224462,'Xenos','Richardson',1,'0000-00-00','Ap #583-5348 Feugiat. Street','non@porta.org',4794841386,4078464178,2,1),(94,1,8,3,6627710223,'Zachery','Myers',2,'0000-00-00','Ap #953-9203 Phasellus Road','magnis@malesuada.co.uk',8192198143,5309275394,1,2),(95,3,8,2,6088658955,'Hop','Ortega',1,'0000-00-00','2246 Vel Rd.','mi.tempor@vitae.co.uk',6876384414,5640853747,1,2),(96,1,7,2,7129326854,'Mira','Cobb',2,'0000-00-00','P.O. Box 259, 1845 Ultrices Avenue','nibh@semegetmassa.com',5685597065,8148841864,3,1),(97,2,10,3,6994639715,'Leila','Vang',2,'0000-00-00','P.O. Box 146, 6492 Euismod St.','mollis.dui@convallisligulaDonec.com',8660260017,4909675040,3,2),(98,3,7,2,7835228961,'Blake','Herring',1,'0000-00-00','182-1750 Magna. Rd.','Nam.porttitor@nec.net',5332574324,4700484635,2,2),(99,4,8,3,6904915303,'Xantha','Caldwell',2,'0000-00-00','P.O. Box 936, 9476 Auctor Rd.','per@varius.edu',7735524006,4911196756,1,1),(100,4,10,3,7258635329,'Lesley','Fuller',1,'0000-00-00','8731 Ut Road','eget@vitaesemperegestas.net',7915761004,8836431779,2,2);
/*!40000 ALTER TABLE `medico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `medicossactivos`
--

DROP TABLE IF EXISTS `medicossactivos`;
/*!50001 DROP VIEW IF EXISTS `medicossactivos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `medicossactivos` (
  `Id_Medico` bigint(20),
  `nom_Medico` varchar(50),
  `nom_Estado` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `operario`
--

DROP TABLE IF EXISTS `operario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operario` (
  `Id_Operario` bigint(20) NOT NULL AUTO_INCREMENT,
  `FKId_Tipo_Doc` bigint(20) NOT NULL,
  `num_doc_Oper` bigint(20) NOT NULL,
  `nom_Oper` varchar(50) NOT NULL,
  `apell_Oper` varchar(50) NOT NULL,
  `FKId_Genero` bigint(20) NOT NULL,
  `fecha_nac_Oper` date NOT NULL,
  `direc_Oper` varchar(255) DEFAULT 'Sin Dirección',
  `email_Oper` varchar(255) DEFAULT 'Sin Email',
  `telf_fijo_Oper` bigint(20) NOT NULL,
  `telf_cel_Oper` bigint(20) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Operario`),
  UNIQUE KEY `num_doc_Oper` (`num_doc_Oper`),
  KEY `tipodocoper` (`FKId_Tipo_Doc`),
  KEY `generocoper` (`FKId_Genero`),
  KEY `estadocoper` (`FKId_Estado`),
  CONSTRAINT `estadocoper` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`),
  CONSTRAINT `generocoper` FOREIGN KEY (`FKId_Genero`) REFERENCES `genero` (`Id_Genero`),
  CONSTRAINT `tipodocoper` FOREIGN KEY (`FKId_Tipo_Doc`) REFERENCES `tipo_documento` (`Id_Tipo_Doc`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operario`
--

LOCK TABLES `operario` WRITE;
/*!40000 ALTER TABLE `operario` DISABLE KEYS */;
INSERT INTO `operario` VALUES (1,2,619774124,'Brenda','Holmes',2,'0000-00-00','Apdo.:198-6956 Quisque Carretera','ultricies.ornare@necanteblandit.com',6,5,1),(2,1,688716764,'Amir','Leonard',1,'0000-00-00','Apartado núm.: 526, 9569 Nullam Carretera','cursus.Nunc.mauris@sedturpis.net',6,6,1),(3,3,485076081,'Kevyn','Manning',1,'0000-00-00','1454 Metus Calle','egestas.a@dignissimmagna.ca',5,3,2),(4,1,439455032,'Hedwig','Mcdonald',2,'0000-00-00','Apdo.:169-6035 Varius. Calle','rhoncus.Proin@acorci.edu',5,8,1),(5,1,519706475,'Cally','Rosales',2,'0000-00-00','Apartado núm.: 647, 421 Malesuada C/','dolor@Maecenas.net',6,10,2),(6,2,615273338,'Bo','Bryant',2,'0000-00-00','7319 Nulla C/','egestas.a.scelerisque@etpedeNunc.ca',2,6,2),(7,1,465782298,'Ray','Manning',2,'0000-00-00','5466 Natoque Calle','dui@incursus.ca',8,8,1),(8,1,713757785,'Larissa','William',1,'0000-00-00','Apdo.:659-9223 Vitae, Av.','sit@Utnecurna.net',10,4,1),(9,2,745025534,'Quemby','Hodge',1,'0000-00-00','5851 Cursus Calle','accumsan.laoreet.ipsum@Namconsequat.ca',9,6,2),(10,1,628739714,'Deborah','Mooney',2,'0000-00-00','Apdo.:593-3406 Lacus. Ctra.','Sed.id.risus@aliquetProin.ca',1,2,1),(11,2,745791960,'Tamara','Cantrell',1,'0000-00-00','Apartado núm.: 846, 945 Egestas, C.','inceptos.hymenaeos@inmolestie.com',1,1,2),(12,1,730196721,'Karly','Tyson',1,'0000-00-00','501-9169 Mollis Avda.','tellus.Aenean@justofaucibus.co.uk',10,6,2),(13,2,404408883,'Alisa','Randall',2,'0000-00-00','Apdo.:463-4028 Ullamcorper C.','et.magnis.dis@montes.org',4,8,2),(14,1,707169788,'Paula','Moran',1,'0000-00-00','Apartado núm.: 757, 6595 Placerat ','arcu.Sed.et@sit.org',9,7,2),(15,2,418726887,'Julian','Ross',2,'0000-00-00','506-4412 Purus, Avda.','enim.sit.amet@Praesent.ca',9,2,2),(16,1,634482154,'Katell','Walter',2,'0000-00-00','6817 Mus. C.','tempor@ProinvelitSed.com',6,5,1),(17,1,741179013,'Ina','Stanton',2,'0000-00-00','Apartado núm.: 365, 4827 Sem. ','sapien.imperdiet.ornare@tortor.edu',5,1,1),(18,3,661772460,'Xandra','Cervantes',1,'0000-00-00','Apdo.:773-988 Eget Calle','erat.eget@quisarcu.org',8,7,2),(19,2,428655786,'Dolan','Wall',1,'0000-00-00','Apartado núm.: 443, 3088 Elit. Avda.','Donec.feugiat@etmalesuada.ca',2,2,2),(20,1,750024115,'Macey','Hatfield',1,'0000-00-00','Apartado núm.: 274, 4274 Eleifend Avenida','enim@luctus.com',3,9,1),(21,2,675203601,'Kirk','Golden',1,'0000-00-00','Apartado núm.: 235, 2876 Dictum Calle','nec@Nullamnisl.edu',1,6,2),(22,3,438626938,'Mercedes','Blackburn',1,'0000-00-00','2970 Enim Avenida','ultrices@Proinvelit.ca',6,4,1),(23,1,636283716,'Hanna','Barlow',1,'0000-00-00','5261 Est, Avenida','massa@nonmassa.org',2,8,1),(24,1,611456293,'Brianna','Robinson',1,'0000-00-00','Apdo.:973-7140 Duis C.','porttitor.vulputate.posuere@posuereat.ca',5,6,1),(25,2,427604268,'Lacota','Copeland',2,'0000-00-00','Apartado núm.: 621, 994 Egestas ','dis@pede.edu',4,3,1),(26,3,433695857,'Karina','Meyer',2,'0000-00-00','271-6885 Sem. C/','nisi@aauctornon.org',2,10,2),(27,2,610572563,'Arthur','Thomas',2,'0000-00-00','Apdo.:886-3337 Nec Ctra.','Sed@urna.net',6,9,2),(28,1,442585229,'Halee','Forbes',1,'0000-00-00','Apdo.:456-2357 Mauris Avenida','felis@posuereat.ca',5,1,2),(29,2,539878610,'Clark','Gamble',1,'0000-00-00','809-2020 Dapibus Calle','mi.eleifend@enimdiamvel.org',6,7,2),(30,3,757939490,'Flavia','Workman',2,'0000-00-00','359-6148 Facilisis Av.','Sed.nulla.ante@inlobortis.net',1,1,1),(31,3,482661329,'Alden','Clark',2,'0000-00-00','Apdo.:652-7212 Nunc Carretera','est.Mauris@ullamcorper.com',9,4,1),(32,1,670340936,'Farrah','Decker',2,'0000-00-00','Apartado núm.: 223, 7642 Sem, ','a.feugiat.tellus@elitpellentesquea.edu',3,6,2),(33,2,628322977,'Keefe','Jarvis',1,'0000-00-00','Apartado núm.: 563, 8007 Et Ctra.','nec@anteMaecenas.co.uk',7,1,1),(34,3,505476041,'Oleg','Parsons',1,'0000-00-00','534 Feugiat. Calle','tempor.lorem@magnamalesuadavel.net',10,9,1),(35,1,781169162,'Candice','Clemons',1,'0000-00-00','243 Nec, Av.','Suspendisse.sed@quispede.org',3,2,1),(36,1,423297232,'Halla','Hampton',2,'0000-00-00','3371 Imperdiet Carretera','gravida.molestie.arcu@idblanditat.ca',1,4,2),(37,2,486237305,'Reece','Crawford',1,'0000-00-00','Apartado núm.: 521, 3759 Ipsum Avenida','enim@nonleo.edu',10,7,1),(38,3,412778239,'Otto','Rocha',1,'0000-00-00','Apartado núm.: 698, 6975 Elementum, C/','lacus.varius.et@Sed.org',5,8,2),(39,2,691536905,'Noelani','Pruitt',2,'0000-00-00','9590 Egestas. C.','malesuada.vel@eratvolutpatNulla.net',9,6,2),(40,3,528131761,'Richard','Mccoy',1,'0000-00-00','3544 Id ','velit.Sed.malesuada@massa.edu',4,9,1),(41,1,751076828,'Kelsie','Beck',1,'0000-00-00','Apartado núm.: 944, 8869 Nulla Avenida','mattis.ornare@Maurisvestibulumneque.edu',1,9,1),(42,2,526249310,'Amela','Maynard',2,'0000-00-00','847 Tincidunt C.','Morbi.quis.urna@quisurna.org',4,7,1),(43,3,598983100,'Sydney','Mclaughlin',2,'0000-00-00','Apdo.:265-6579 Neque Avenida','nec.cursus@sitametconsectetuer.co.uk',6,5,1),(44,3,640243698,'Amena','Pena',2,'0000-00-00','Apdo.:807-6430 Morbi Carretera','nec.luctus@sedorci.org',2,2,2),(45,2,778968809,'Karleigh','Ferrell',2,'0000-00-00','4147 Elit, Av.','et.lacinia.vitae@sapienimperdiet.ca',10,4,1),(46,3,721905849,'Cooper','Lloyd',2,'0000-00-00','230 Sagittis C.','lacus@Nunclectus.com',1,8,2),(47,3,774725229,'Tamara','Adkins',1,'0000-00-00','Apartado núm.: 651, 2897 Nullam Calle','Etiam@adipiscingnonluctus.org',1,7,1),(48,2,782960841,'Shea','Mays',2,'0000-00-00','3056 Vivamus ','Proin@mattis.edu',5,1,2),(49,3,507125736,'Minerva','Lindsay',1,'0000-00-00','Apdo.:636-3957 Cum Av.','tempus@augue.com',8,1,1),(50,1,550328524,'Edward','Bond',2,'0000-00-00','2000 Eu Avda.','hendrerit.Donec@loremipsumsodales.net',7,7,1),(51,1,714100549,'Sydney','Kidd',2,'0000-00-00','186-5091 Lobortis, ','lobortis.ultrices@vestibulummassa.ca',2,2,1),(52,1,691366102,'Ocean','Randolph',1,'0000-00-00','Apdo.:915-6339 Non, C/','tortor@nonleoVivamus.co.uk',2,8,1),(53,2,581269374,'Lucas','Howe',1,'0000-00-00','585-2074 Ipsum. C.','lectus.justo@magnisdis.org',6,4,1),(54,2,561946306,'Lysandra','Cochran',1,'0000-00-00','6263 Eleifend C.','risus@utquamvel.org',3,5,1),(55,2,659479740,'MacKensie','Horton',1,'0000-00-00','6741 Vestibulum Ctra.','ornare@sem.com',10,5,2),(56,3,541187085,'Macey','Saunders',1,'0000-00-00','6875 Facilisis Calle','amet@malesuadavelconvallis.net',3,5,2),(57,2,645642358,'Hector','Mccoy',1,'0000-00-00','Apartado núm.: 290, 5515 Felis ','Quisque.nonummy@malesuadafamesac.com',8,10,2),(58,2,649757075,'Phillip','Riddle',2,'0000-00-00','Apdo.:148-7510 Lorem Calle','eget.ipsum@estmauris.co.uk',3,8,2),(59,3,404386185,'Chaney','Mcpherson',2,'0000-00-00','653-3922 Ac Ctra.','convallis.est.vitae@Nuncpulvinar.edu',10,4,2),(60,2,659463586,'Emma','King',2,'0000-00-00','Apartado núm.: 785, 4397 Leo. Calle','blandit.mattis.Cras@tellus.edu',1,6,2),(61,3,690846508,'Stella','Haney',2,'0000-00-00','5901 Ultrices. Av.','Donec@augueeu.co.uk',5,6,1),(62,2,520050631,'Dexter','Walls',2,'0000-00-00','494-2379 Nec ','sit.amet@ornare.edu',9,9,2),(63,1,439970833,'Charlotte','Fernandez',1,'0000-00-00','Apdo.:649-2580 Fusce Av.','venenatis.a@eutemporerat.net',7,2,2),(64,2,798689757,'Dolan','Mays',2,'0000-00-00','5672 Et, ','magnis@egestasligula.edu',7,6,1),(65,2,483475795,'Isaiah','Wolfe',1,'0000-00-00','Apdo.:839-8463 Nec C/','fermentum.convallis@malesuadaIntegerid.edu',6,3,2),(66,1,790109419,'Alea','Frank',2,'0000-00-00','274-1607 Pellentesque ','hymenaeos.Mauris.ut@malesuada.edu',5,5,2),(67,3,564429151,'Barrett','Farley',1,'0000-00-00','Apdo.:544-9223 Ante Av.','Donec.feugiat.metus@aliquetodio.ca',6,9,1),(68,3,493638121,'Bernard','Gentry',1,'0000-00-00','307-8930 Aliquet. Av.','sem.eget.massa@asollicitudin.edu',6,9,2),(69,2,420137760,'Iris','Velazquez',2,'0000-00-00','Apdo.:879-9560 Justo. C/','mauris.a.nunc@eutemporerat.edu',7,4,1),(70,2,727751842,'Abigail','Rush',2,'0000-00-00','461-2725 Nostra, ','Aenean.eget.magna@et.edu',7,6,2),(71,3,485264116,'Jaquelyn','Mcintyre',1,'0000-00-00','687-2711 Magnis Avenida','Donec@tincidunt.ca',7,7,1),(72,3,633854619,'Judith','Booth',1,'0000-00-00','995-164 Non Calle','euismod.mauris.eu@velitinaliquet.ca',1,7,2),(73,1,651982102,'Brennan','Bailey',1,'0000-00-00','Apdo.:834-4288 Tincidunt, C.','nec@cubilia.co.uk',10,1,2),(74,3,411679050,'Clare','Roberts',1,'0000-00-00','Apartado núm.: 611, 3813 Semper Carretera','Donec.egestas@Quisqueimperdiet.co.uk',4,8,2),(75,1,453063296,'Jayme','Blake',1,'0000-00-00','Apdo.:318-3585 Nunc C.','orci.Donec@Donectempus.net',4,7,2),(76,3,715731658,'Ira','Bryan',2,'0000-00-00','Apdo.:660-9674 Risus. C/','Donec@ridiculusmus.co.uk',7,3,2),(77,2,665121003,'Yoko','Macdonald',2,'0000-00-00','Apdo.:413-4338 Donec Calle','eget.tincidunt.dui@congue.net',6,4,2),(78,1,743476091,'Kirk','Merritt',1,'0000-00-00','Apdo.:109-3611 Blandit Calle','metus.Aliquam.erat@Maurisnondui.edu',8,10,1),(79,3,567621117,'Rhoda','Slater',2,'0000-00-00','471-3826 Rutrum Av.','a.arcu@elit.org',1,1,1),(80,2,718084777,'Gregory','Reese',2,'0000-00-00','Apartado núm.: 479, 4966 Mattis Ctra.','tellus.Nunc@rhoncus.co.uk',4,4,1),(81,2,644604795,'Ivana','Montoya',2,'0000-00-00','149-3340 Eleifend. ','non@adipiscingMaurismolestie.net',6,1,2),(82,3,485489373,'Hall','Espinoza',2,'0000-00-00','Apdo.:484-3389 Gravida Carretera','feugiat.Sed.nec@vulputateveliteu.co.uk',6,4,1),(83,3,682072531,'Richard','Schultz',2,'0000-00-00','Apdo.:976-4610 Donec Avenida','neque.non.quam@primisin.ca',9,5,2),(84,1,402373350,'Drake','Jarvis',1,'0000-00-00','Apdo.:326-8417 Varius Ctra.','quam@rhoncusDonec.co.uk',1,6,1),(85,2,693524804,'Anastasia','Hewitt',1,'0000-00-00','3916 Luctus Avenida','Donec.nibh.Quisque@Integer.com',4,3,1),(86,2,636187898,'Aline','Wilson',2,'0000-00-00','3489 Tellus. C/','pellentesque@ullamcorpereueuismod.com',3,6,2),(87,2,672698545,'Cruz','Mendoza',1,'0000-00-00','Apartado núm.: 329, 3713 Aenean Ctra.','in@velitAliquam.com',3,3,1),(88,3,411943543,'Blair','Frank',1,'0000-00-00','114-7938 Sit Ctra.','ullamcorper.nisl@Cras.co.uk',5,10,2),(89,3,742700833,'Xanthus','Mckay',2,'0000-00-00','Apartado núm.: 684, 2106 Duis ','sagittis.Duis.gravida@odioNam.net',1,10,2),(90,3,593146925,'Meredith','Trevino',2,'0000-00-00','Apartado núm.: 623, 9696 Aliquam Calle','ultricies.sem@necdiam.co.uk',6,5,1),(91,2,694731585,'Solomon','Reeves',1,'0000-00-00','Apdo.:947-6177 Risus. Carretera','Nam@egetmetusIn.edu',6,5,1),(92,3,750502838,'Piper','Briggs',2,'0000-00-00','605-7581 Elit, Carretera','sagittis@anuncIn.com',5,6,1),(93,1,419540528,'Aretha','Wilson',1,'0000-00-00','Apdo.:851-6121 Scelerisque Av.','tellus.lorem.eu@ut.com',1,2,1),(94,2,471293694,'Cain','Beasley',2,'0000-00-00','Apartado núm.: 677, 8991 Arcu. Avda.','a.sollicitudin.orci@atpede.com',2,9,1),(95,3,644854241,'Branden','Hines',2,'0000-00-00','Apdo.:773-4481 Consectetuer Av.','ac@faucibusorciluctus.ca',5,9,1),(96,1,535858592,'Sybill','Bennett',1,'0000-00-00','5078 Iaculis Carretera','eu.sem.Pellentesque@massarutrum.ca',9,1,1),(97,3,612781872,'Morgan','Stanton',1,'0000-00-00','Apdo.:353-3213 Metus Av.','cursus.in@nislsem.edu',10,8,2),(98,1,658217926,'Anne','Boyle',1,'0000-00-00','594-5036 Nostra, ','lacinia.Sed.congue@sem.ca',1,5,2),(99,2,476411258,'Austin','Ashley',2,'0000-00-00','204-3708 Mauris ','enim.condimentum.eget@Aliquamornare.ca',3,7,1),(100,3,766304155,'Andrew','Whitley',1,'0000-00-00','372-737 Quis, C.','enim.Mauris.quis@mifelis.org',4,1,2),(101,3,617316360,'Zenaida','Woodard',2,'0000-00-00','Apdo.:295-9490 Magna. Av.','eros.Proin.ultrices@blanditcongue.co.uk',8,8,2);
/*!40000 ALTER TABLE `operario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `operariossactivos`
--

DROP TABLE IF EXISTS `operariossactivos`;
/*!50001 DROP VIEW IF EXISTS `operariossactivos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `operariossactivos` (
  `Id_Operario` bigint(20),
  `nom_Oper` varchar(50),
  `nom_Estado` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `paciente`
--

DROP TABLE IF EXISTS `paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paciente` (
  `Id_Paciente` bigint(20) NOT NULL AUTO_INCREMENT,
  `FKId_Tipo_Doc` bigint(20) NOT NULL,
  `num_doc_Pac` bigint(20) NOT NULL,
  `nom_Paciente` varchar(50) NOT NULL,
  `apell_Paciente` varchar(50) NOT NULL,
  `FKId_Genero` bigint(20) NOT NULL,
  `fecha_nac_Pac` date NOT NULL,
  `direc_Paciente` varchar(255) DEFAULT 'Sin Dirección',
  `email_Paciente` varchar(255) DEFAULT 'Sin Email',
  `telf_fijo_Pac` bigint(20) NOT NULL,
  `telf_cel_Pac` bigint(20) NOT NULL,
  `Enfermedad` varchar(255) DEFAULT 'Sin Enfermedad',
  `Medicamentos` varchar(255) DEFAULT 'Sin Medicamentos',
  `Alergia` varchar(255) DEFAULT 'Sin Alergias',
  `FKId_Estado` bigint(20) NOT NULL,
  `FKId_EPS` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Paciente`),
  UNIQUE KEY `num_doc_Pac` (`num_doc_Pac`),
  KEY `tipodocpac` (`FKId_Tipo_Doc`),
  KEY `generopac` (`FKId_Genero`),
  KEY `estadopac` (`FKId_Estado`),
  KEY `epspaciente` (`FKId_EPS`),
  CONSTRAINT `paciente_ibfk_1` FOREIGN KEY (`FKId_EPS`) REFERENCES `eps` (`Id_EPS`),
  CONSTRAINT `estadopac` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`),
  CONSTRAINT `generopac` FOREIGN KEY (`FKId_Genero`) REFERENCES `genero` (`Id_Genero`),
  CONSTRAINT `tipodocpac` FOREIGN KEY (`FKId_Tipo_Doc`) REFERENCES `tipo_documento` (`Id_Tipo_Doc`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente`
--

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` VALUES (1,1,54265955,'Malik','Hoffman',2,'0000-00-00','P.O. Box 459, 7830 Eget Street','risus@dolorvitae.co.uk',24815229,16720225,'risus. Morbi metus.','elit,','In nec',1,5),(2,2,77676916,'Alfonso','Beach',2,'0000-00-00','P.O. Box 621, 9061 Dui. St.','nisl@purus.co.uk',49935137,16190315,'Donec tempor,','aliquet','dictum sapien. Aenean massa. Integer vitae nibh. Donec est',2,5),(3,2,76214427,'Alexa','Rocha',2,'0000-00-00','Ap #771-1817 Lobortis St.','Sed.eu@nullavulputatedui.net',41982977,16841202,'porttitor','mauris. Suspendisse aliquet','convallis convallis dolor. Quisque tincidunt pede',2,3),(4,3,67991868,'Carter','Payne',1,'0000-00-00','751-8799 Lorem St.','Integer.urna.Vivamus@variusNamporttitor.org',8582032,16220925,'libero. Donec','sodales purus,','Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim.',1,4),(5,3,48109196,'Beau','Crawford',1,'0000-00-00','Ap #628-6008 Ipsum Road','ut.pellentesque.eget@sagittis.ca',8720501,16911015,'Aliquam','ac sem ut','pede ac urna. Ut',1,5),(6,2,66493014,'Leroy','Levy',2,'0000-00-00','P.O. Box 320, 5233 Lacus. Av.','Pellentesque.ut.ipsum@nibhAliquamornare.com',9341199,16131001,'lorem lorem,','nulla. Integer','lacinia vitae, sodales at,',2,3),(7,1,73728277,'Tatum','Gilmore',2,'0000-00-00','P.O. Box 352, 2643 Ipsum Rd.','dignissim.magna@afelisullamcorper.edu',44810124,16960114,'convallis convallis','gravida sagittis. Duis','Integer urna. Vivamus molestie dapibus ligula.',1,3),(8,1,78832944,'Maggie','Gaines',1,'0000-00-00','P.O. Box 981, 7208 Libero Rd.','sit.amet.ultricies@Crasvulputatevelit.org',30666545,16380414,'faucibus orci luctus','elementum, dui quis','scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed,',1,3),(9,1,51222537,'Conan','Hebert',1,'0000-00-00','Ap #447-9525 Sed St.','cursus.et.magna@odioNaminterdum.ca',19935954,16000211,'odio semper cursus.','fermentum','vulputate dui, nec tempus mauris erat eget',2,2),(10,1,65273571,'Amos','Combs',2,'0000-00-00','Ap #590-661 Posuere Road','hendrerit.id@risusIn.com',35155914,16871122,'Mauris eu turpis.','arcu.','libero. Morbi accumsan laoreet',2,2),(11,2,75512600,'Jack','Stephens',2,'0000-00-00','108-3401 Tristique Ave','malesuada.vel@tempusmauris.co.uk',31591484,16651106,'mi','suscipit nonummy. Fusce','viverra. Maecenas',1,4),(12,1,68305066,'Chelsea','Goff',1,'0000-00-00','Ap #596-9458 Integer St.','convallis@Nullam.co.uk',41703514,16591016,'massa','Aliquam','enim, sit',2,1),(13,2,63980542,'Larissa','Santos',2,'0000-00-00','P.O. Box 439, 7412 Dolor Ave','Donec.luctus.aliquet@suscipit.edu',15701629,16880801,'turpis','orci quis lectus.','convallis in, cursus et, eros.',2,2),(14,3,71290623,'Catherine','Martinez',2,'0000-00-00','P.O. Box 303, 9552 Dui. Rd.','est.Nunc@lorem.edu',6274959,16420312,'mauris blandit mattis.','natoque penatibus','est,',1,2),(15,2,44739466,'Connor','Church',1,'0000-00-00','Ap #416-616 Ut Road','ipsum@semNulla.co.uk',37425073,16220708,'vulputate, posuere vulputate,','ultrices a,','odio, auctor',1,1),(16,2,52708666,'Guinevere','Sanchez',2,'0000-00-00','5457 Morbi Street','ullamcorper.Duis@Donectempuslorem.co.uk',38114994,16801130,'urna','dui quis accumsan','dictum eu, placerat eget, venenatis a, magna. Lorem ipsum',1,3),(17,2,70968171,'Ori','Turner',1,'0000-00-00','Ap #171-4757 Eleifend Street','Fusce@utcursusluctus.org',47543695,16710724,'feugiat','vel arcu.','eget ipsum.',2,1),(18,1,40844210,'Lance','Bright',2,'0000-00-00','3588 Nunc Rd.','Donec@liberoduinec.edu',8646776,16360622,'semper','augue. Sed','ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus.',2,4),(19,3,77017715,'Daniel','Morrow',1,'0000-00-00','1559 Sed Street','felis.adipiscing.fringilla@temporlorem.org',46714065,16060211,'augue eu tellus.','mi eleifend','ac, feugiat non, lobortis',2,1),(20,3,43760435,'Griffith','Newton',1,'0000-00-00','6150 Mattis. St.','non.ante.bibendum@ullamcorper.edu',6315211,16380117,'odio. Etiam ligula','ullamcorper magna.','aliquet, sem ut cursus luctus, ipsum',2,4),(21,1,73579511,'William','Avery',2,'0000-00-00','Ap #411-7601 Proin Av.','scelerisque.dui.Suspendisse@ligulaAenean.co.uk',18498684,16250408,'dolor','sem, consequat','vel, vulputate eu, odio. Phasellus at',1,3),(22,2,55595961,'Deacon','Hopkins',2,'0000-00-00','P.O. Box 681, 9125 Vulputate, Road','ante@vitaealiquetnec.net',8224766,16341002,'neque','tellus','blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus',2,5),(23,3,70447272,'Renee','Hill',2,'0000-00-00','947-1438 Ac Rd.','eu.tellus.Phasellus@Pellentesquehabitantmorbi.com',20078436,16970827,'penatibus','Mauris ut quam','a, arcu. Sed et libero. Proin mi. Aliquam gravida',1,2),(24,2,75696665,'Bruce','Kent',2,'0000-00-00','P.O. Box 422, 5646 Egestas Street','malesuada.ut.sem@erat.ca',36088039,16420721,'lorem, vehicula','id, erat.','volutpat ornare, facilisis',1,2),(25,1,78784108,'Irma','Estes',1,'0000-00-00','974-8138 Pede Street','eu@DonecegestasDuis.com',28484865,16800202,'scelerisque scelerisque dui.','imperdiet','lacinia at, iaculis quis,',1,4),(26,3,65793473,'Raya','Pierce',1,'0000-00-00','P.O. Box 425, 8269 Ipsum St.','metus@ac.com',49267192,16061216,'Curabitur ut odio','tellus.','ac mi eleifend egestas. Sed pharetra, felis eget',2,5),(27,3,48605467,'Charles','Berger',1,'0000-00-00','Ap #772-2360 Mauris Street','sed.consequat.auctor@est.net',24024996,16120509,'pharetra. Quisque','nec, euismod in,','semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae',1,2),(28,3,45832520,'Scarlet','Kramer',2,'0000-00-00','471-9273 Aliquam St.','quis.diam.Pellentesque@ipsumsodales.org',42680885,16371026,'accumsan','purus,','vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at',1,3),(29,3,41053180,'Francesca','Rivera',2,'0000-00-00','Ap #394-6383 Sollicitudin Ave','tempus.non.lacinia@tristiqueac.edu',20961568,16260901,'ac ipsum. Phasellus','posuere cubilia','blandit at, nisi. Cum sociis natoque',2,5),(30,2,68185056,'Judah','Frank',1,'0000-00-00','9495 Nullam Ave','quam.Curabitur.vel@eu.net',42591974,16530916,'dolor. Nulla','arcu eu odio','ultrices. Duis volutpat nunc sit amet metus. Aliquam',1,2),(31,3,56154379,'Amir','Glass',2,'0000-00-00','716-3956 Lobortis Rd.','mauris.ut.mi@MaurismagnaDuis.com',37287859,16970402,'odio sagittis semper.','neque pellentesque massa','adipiscing lobortis risus. In mi pede, nonummy',1,3),(32,1,40158145,'Steven','Clemons',2,'0000-00-00','P.O. Box 715, 6226 Erat Street','sodales.elit.erat@lobortisquispede.org',34767773,16890928,'Fusce mi lorem,','Integer','nec, euismod in, dolor.',1,2),(33,1,74560462,'Ira','Becker',1,'0000-00-00','6045 At, Rd.','hendrerit.consectetuer@non.net',27540151,16780905,'pede','natoque penatibus','sed, hendrerit a,',1,5),(34,3,66325218,'Samuel','Conley',1,'0000-00-00','661-7732 Consectetuer, Avenue','lectus.pede@dictumPhasellus.co.uk',6454573,16870503,'senectus','amet,','a, aliquet',2,1),(35,3,51999105,'Zeus','Sargent',2,'0000-00-00','4431 Vestibulum Avenue','et.tristique.pellentesque@facilisismagnatellus.co.uk',32121280,16861101,'at augue','imperdiet','nec ante blandit viverra. Donec tempus, lorem',1,1),(36,1,60769082,'Anne','Reynolds',2,'0000-00-00','523-2882 Sodales Ave','sed.pede@Innecorci.com',42381288,16990708,'eros','metus sit','semper pretium',1,1),(37,2,54001176,'Lesley','Ayers',1,'0000-00-00','P.O. Box 333, 9569 Tincidunt Av.','nulla@Nunclectus.ca',31331769,16740401,'adipiscing elit. Curabitur','Curabitur','a nunc.',2,3),(38,2,76749567,'Isabelle','Kelley',1,'0000-00-00','702-9879 Erat Street','lobortis@Fuscedolor.com',42192257,16150722,'a,','at, velit. Cras','molestie tellus.',2,4),(39,3,66111239,'Dominique','Warner',1,'0000-00-00','9452 Arcu. St.','eros.Proin.ultrices@luctusaliquetodio.ca',48951818,16650419,'Aliquam','enim,','velit justo nec',2,2),(40,1,60330160,'Lareina','David',1,'0000-00-00','270-5927 Nunc Ave','orci@vitae.org',31020628,16201211,'lobortis','vitae nibh. Donec','eu tellus eu augue',2,2),(41,3,79695750,'Ruth','Alexander',2,'0000-00-00','Ap #208-5566 Magnis St.','sagittis.augue.eu@turpis.edu',30357878,16920824,'vel lectus.','dignissim.','orci lacus vestibulum',1,2),(42,2,61121982,'Keiko','Klein',2,'0000-00-00','P.O. Box 285, 4927 Proin Ave','rutrum@tortornibh.org',41329903,16121207,'suscipit, est ac','natoque penatibus et','sem elit, pharetra ut, pharetra sed, hendrerit a,',1,1),(43,3,79961236,'Ethan','Valenzuela',2,'0000-00-00','777-4224 Tellus Av.','Mauris.magna@Proinultrices.ca',28995966,16210721,'lectus. Nullam suscipit,','Suspendisse aliquet molestie','magna. Duis dignissim tempor arcu. Vestibulum ut eros non',1,4),(44,2,79676719,'Brett','Gallegos',1,'0000-00-00','Ap #438-6231 Nec, Street','interdum.enim@rutrumnon.ca',25177267,16850606,'eu dui.','est, vitae sodales','interdum. Sed',2,2),(45,1,52228961,'Jerome','Lara',1,'0000-00-00','Ap #552-7875 Blandit Av.','vitae.odio.sagittis@enimnectempus.org',16290800,16560222,'habitant morbi tristique','urna convallis erat,','arcu. Vestibulum ante ipsum primis in faucibus',1,3),(46,1,70353742,'Cheyenne','Morales',1,'0000-00-00','8670 Eu Rd.','id.libero.Donec@eget.ca',26227368,16461202,'quam. Curabitur vel','natoque penatibus et','diam dictum sapien. Aenean',1,4),(47,2,63038858,'Orlando','Nicholson',2,'0000-00-00','2734 Ante. Rd.','luctus@afacilisis.org',10911702,16810823,'Suspendisse dui.','Fusce mi lorem,','dictum eu, eleifend nec, malesuada ut, sem.',1,4),(48,2,58061792,'Jolene','Burgess',2,'0000-00-00','Ap #472-7954 Vel Rd.','cursus@erosNam.co.uk',44362544,16890613,'enim mi','dictum cursus. Nunc','iaculis odio. Nam interdum enim non',2,5),(49,2,58972765,'Chaney','Baird',1,'0000-00-00','902-4657 Tellus St.','erat@semegestas.net',25574621,16541022,'id nunc interdum','sit amet','volutpat. Nulla dignissim. Maecenas ornare egestas ligula.',1,2),(50,1,50017022,'Lysandra','Short',2,'0000-00-00','900-7580 Eu Ave','est@cursusluctus.com',35675581,16000604,'lorem','ullamcorper','nunc. Quisque ornare tortor at risus. Nunc ac sem ut',2,4),(51,2,55027434,'Ulysses','Holmes',2,'0000-00-00','Ap #722-8751 Consectetuer Rd.','blandit@ultricesDuis.edu',5543459,16771128,'vel pede blandit','magna, malesuada vel,','libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus',2,5),(52,3,53748338,'Clark','Delaney',2,'0000-00-00','P.O. Box 454, 1600 Adipiscing Rd.','elit.pellentesque.a@neceuismod.org',15769342,16420703,'posuere cubilia','sit amet ornare','parturient montes, nascetur',1,5),(53,3,52975016,'Fay','Puckett',2,'0000-00-00','663-380 Magna Avenue','eu@Integereulacus.co.uk',11235120,16420213,'mauris. Suspendisse aliquet','augue scelerisque','vestibulum nec, euismod in, dolor. Fusce feugiat.',2,1),(54,1,48661660,'Ralph','Gray',1,'0000-00-00','1271 Enim St.','vel.convallis.in@eueros.net',38536050,16851009,'suscipit','mollis vitae, posuere','magna. Suspendisse tristique neque venenatis lacus.',1,2),(55,1,65562569,'Chloe','Hays',1,'0000-00-00','6885 Cursus Avenue','Lorem.ipsum@Loremipsumdolor.com',22678701,16300401,'ligula.','ac mattis ornare,','consectetuer euismod est arcu ac orci.',2,1),(56,2,44129535,'Callum','Fowler',1,'0000-00-00','529-736 Tortor St.','ipsum@viverra.co.uk',29284329,16370330,'quam a','feugiat placerat velit.','a, auctor non, feugiat nec, diam. Duis',1,2),(57,2,41478241,'Maggie','Foster',2,'0000-00-00','Ap #440-4521 Mus. Rd.','quis@necquamCurabitur.com',17689885,16870414,'Mauris vel turpis.','imperdiet ullamcorper.','sit',2,3),(58,2,78271958,'Margaret','Quinn',1,'0000-00-00','6576 Et Ave','odio.vel@Cum.net',23840063,16080606,'nunc. Quisque ornare','dolor quam, elementum','Ut tincidunt orci quis lectus. Nullam suscipit,',2,1),(59,1,61238880,'Lysandra','Price',1,'0000-00-00','334-9251 Auctor St.','ultricies.sem@velturpisAliquam.co.uk',5189813,16161223,'lobortis, nisi nibh','Mauris blandit enim','habitant morbi tristique',1,3),(60,3,44389650,'Mira','Lamb',1,'0000-00-00','Ap #217-6219 Velit Street','penatibus.et.magnis@enimSed.com',44337460,16750601,'Quisque porttitor eros','neque sed','pretium aliquet,',2,3),(61,1,64506943,'Uta','Irwin',1,'0000-00-00','5756 Nibh. Ave','fames.ac@loremsit.org',7790959,16120713,'Aliquam','sagittis','urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas iaculis',1,1),(62,1,65359395,'Rhonda','Tran',2,'0000-00-00','946 Vestibulum St.','Aliquam.ornare.libero@dapibusligula.co.uk',38316773,16820519,'mattis semper,','ornare','vel lectus. Cum sociis natoque penatibus et magnis',2,5),(63,3,48739352,'Ursula','Melton',1,'0000-00-00','P.O. Box 343, 7912 Curae; Av.','dictum.Phasellus@vel.net',19270447,16111018,'blandit','vel nisl. Quisque','ullamcorper eu, euismod ac, fermentum vel,',1,3),(64,3,76267342,'Maile','Orr',2,'0000-00-00','300-3890 Nisl Av.','lobortis.quam.a@primisin.co.uk',46464877,16710622,'Aliquam ornare,','ac','nunc. Quisque ornare tortor at risus.',2,5),(65,1,57794796,'Aidan','Irwin',1,'0000-00-00','Ap #238-9004 Et St.','eleifend@vehiculaPellentesquetincidunt.co.uk',30369013,16310929,'quis, pede. Suspendisse','quis turpis vitae','Vivamus nisi. Mauris nulla.',2,4),(66,2,74682382,'Sonya','Wyatt',1,'0000-00-00','Ap #168-636 Porttitor St.','ornare@tellusidnunc.co.uk',25373454,16960301,'scelerisque','tincidunt','nulla. In tincidunt congue turpis. In condimentum. Donec',2,4),(67,3,76278085,'Dominic','Harris',2,'0000-00-00','Ap #551-3073 Phasellus Av.','cursus.non.egestas@maurissitamet.ca',18604483,16240313,'sit','ut, sem. Nulla','adipiscing elit. Aliquam auctor, velit eget',1,3),(68,3,77451456,'Laura','Oneill',1,'0000-00-00','816-407 Pellentesque Ave','neque@eleifendnuncrisus.co.uk',16393588,16120707,'eget, dictum','pretium','Mauris non dui nec',1,3),(69,1,67863085,'Maia','Cardenas',1,'0000-00-00','Ap #352-853 Dui. Rd.','consequat@nislNulla.com',40893505,16120410,'Etiam imperdiet dictum','ultricies','pede et risus. Quisque libero lacus, varius et, euismod et,',2,5),(70,3,74279411,'Latifah','Watts',1,'0000-00-00','P.O. Box 861, 7932 Sed St.','sit.amet.consectetuer@magna.net',23145463,16470526,'sem magna','orci. Donec nibh.','non,',2,5),(71,3,46265776,'Jasmine','Cabrera',1,'0000-00-00','602-4307 Hendrerit Road','at.fringilla.purus@ultrices.org',32734524,16940515,'non nisi. Aenean','tellus. Aenean egestas','varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas',1,5),(72,1,54476365,'Shaine','Lyons',1,'0000-00-00','102-8379 Vitae Av.','erat@adlitora.co.uk',48823945,16790608,'egestas','Vivamus sit amet','arcu.',1,3),(73,2,44643348,'Latifah','Guthrie',2,'0000-00-00','563-5319 Sed Ave','fringilla@porttitor.com',7873982,16210119,'eu, eleifend nec,','ornare','nisi dictum augue',1,5),(74,3,78332877,'Blossom','Cote',2,'0000-00-00','7174 Vivamus St.','mauris@leoelementum.ca',12275202,16470425,'sit','eget, volutpat','erat',2,3),(75,1,69002547,'Lana','Mercado',1,'0000-00-00','Ap #778-7226 Ultricies Rd.','faucibus@variuset.edu',36274628,16211018,'Ut nec','lobortis.','augue id ante dictum cursus. Nunc mauris elit,',2,5),(76,2,40653742,'Ezekiel','Mcneil',2,'0000-00-00','2484 Tempor Ave','Maecenas.malesuada.fringilla@lectussit.edu',19579789,16520728,'vel arcu.','a, dui. Cras','sem magna nec quam. Curabitur vel',2,5),(77,1,49563390,'Cherokee','Newton',1,'0000-00-00','945-834 Vel Street','ante.iaculis@In.ca',10341032,16970610,'sodales.','Duis dignissim tempor','vestibulum',2,4),(78,3,69477732,'Linus','Mccarthy',2,'0000-00-00','444-4373 Posuere, Avenue','Nullam.feugiat@maurisMorbi.ca',40580931,16920105,'orci sem','neque','nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas',2,5),(79,3,59406971,'Carolyn','Payne',2,'0000-00-00','Ap #881-7172 Sem Rd.','Nam.consequat.dolor@ipsumPhasellusvitae.com',6453675,16490119,'fringilla','tellus justo','at, libero. Morbi accumsan laoreet ipsum. Curabitur',1,3),(80,3,41011422,'Brendan','Key',1,'0000-00-00','348-4003 Dictum Rd.','quis.pede.Praesent@augueeu.com',16727080,16951109,'ullamcorper,','non quam. Pellentesque','tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec',2,3),(81,3,73853624,'Kimberley','Lynn',2,'0000-00-00','Ap #569-9337 Sed Ave','adipiscing.enim@luctusutpellentesque.co.uk',23317624,16690510,'auctor','risus, at','dui, nec tempus',2,3),(82,2,58909926,'Macaulay','Fowler',2,'0000-00-00','Ap #505-8675 Magna. Rd.','mus.Donec@magnaDuisdignissim.ca',13644520,16141209,'Etiam vestibulum massa','eu','ligula tortor, dictum eu, placerat eget, venenatis',1,4),(83,3,55576730,'Patricia','Galloway',1,'0000-00-00','P.O. Box 946, 5004 Interdum. Road','semper.erat.in@miAliquamgravida.org',33179932,16490112,'nunc','Donec sollicitudin adipiscing','mauris erat eget ipsum. Suspendisse sagittis. Nullam',1,3),(84,2,67603832,'Kevyn','Robles',2,'0000-00-00','Ap #176-3522 Egestas St.','sapien.Nunc@loremsitamet.net',17602984,16880812,'nunc nulla vulputate','bibendum sed, est.','sed orci lobortis augue scelerisque mollis. Phasellus libero',2,4),(85,2,72025890,'Zachery','Higgins',2,'0000-00-00','P.O. Box 999, 3646 Mollis Av.','parturient.montes@dolorQuisque.com',44388623,16391023,'senectus','sociis','Proin eget odio. Aliquam',2,5),(86,1,72707657,'Quyn','Garner',2,'0000-00-00','Ap #485-9830 Duis Ave','tortor@pulvinar.ca',18508932,16250519,'Proin ultrices.','Aenean sed pede','amet, dapibus id, blandit',1,2),(87,1,56013606,'Kieran','Mcclain',1,'0000-00-00','P.O. Box 551, 5505 Scelerisque Street','ipsum@risusDuisa.net',32429124,16250108,'amet','venenatis a, magna.','Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque',2,3),(88,2,50713749,'Jana','Price',1,'0000-00-00','9862 Adipiscing, St.','rutrum@volutpatornarefacilisis.ca',49566201,16730723,'dui quis accumsan','fringilla. Donec','Morbi neque',2,4),(89,2,46482693,'Kendall','Mills',1,'0000-00-00','2148 Aliquam Road','facilisis.lorem@enimmitempor.net',47980818,16070524,'lobortis. Class aptent','Cum sociis','In mi',1,4),(90,3,71691656,'Herman','Grant',2,'0000-00-00','Ap #358-5365 Libero. Road','lectus@nascetur.ca',34706994,16830803,'egestas.','Nam','Phasellus nulla. Integer vulputate, risus a ultricies',2,3),(91,3,41361295,'Kristen','Gordon',2,'0000-00-00','807-3216 A St.','Sed.et.libero@tincidunt.edu',48757258,16610404,'inceptos hymenaeos.','Sed','imperdiet nec, leo.',2,1),(92,3,63080546,'Keith','Eaton',1,'0000-00-00','P.O. Box 517, 7234 Sed, Av.','sit.amet.consectetuer@Nuncmauris.ca',47439803,16020103,'ullamcorper, velit','vestibulum','vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum',2,2),(93,3,75305393,'Sierra','Stevens',1,'0000-00-00','Ap #490-1073 Duis Rd.','non.enim.commodo@Inornare.co.uk',9129720,16560107,'Phasellus at','sagittis semper.','porttitor scelerisque',2,1),(94,3,66414806,'Anthony','Donaldson',1,'0000-00-00','9498 Purus, Av.','Duis.dignissim@musProin.com',8608895,16760119,'Nullam','elementum,','ut erat. Sed nunc',1,5),(95,3,51471022,'Tanisha','Harper',1,'0000-00-00','Ap #449-168 Consectetuer Av.','amet.consectetuer.adipiscing@velarcueu.co.uk',47622788,16481113,'ut, pellentesque eget,','at, iaculis','Nullam scelerisque neque sed sem egestas',1,1),(96,2,49328804,'Mary','Perry',2,'0000-00-00','1785 Accumsan Avenue','quis.massa.Mauris@ac.net',28930063,16180629,'vestibulum massa rutrum','placerat velit.','Integer',1,5),(97,2,42762605,'Clark','Fuentes',2,'0000-00-00','P.O. Box 684, 4290 Morbi St.','hendrerit.neque.In@maurisrhoncusid.org',40602624,16330121,'Cras dolor','ultricies','Aenean',2,3),(98,2,47718337,'Merrill','Foster',2,'0000-00-00','P.O. Box 851, 9769 Tincidunt Av.','vel@nibhDonec.co.uk',44032517,16431016,'accumsan','nunc','hendrerit neque. In ornare sagittis felis. Donec tempor, est ac',2,2),(99,1,47068290,'Mercedes','Lopez',2,'0000-00-00','Ap #226-9661 Ornare Ave','Nulla@hendreritDonecporttitor.edu',11149111,16220504,'interdum. Curabitur','tempus non, lacinia','ligula. Nullam enim. Sed',1,3),(100,3,67859711,'Tatyana','Freeman',2,'0000-00-00','P.O. Box 983, 1138 Velit. Rd.','Quisque@orciUt.edu',48199691,16640113,'aliquet','pellentesque,','lacus. Ut nec urna',1,5);
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `pacientesactivos`
--

DROP TABLE IF EXISTS `pacientesactivos`;
/*!50001 DROP VIEW IF EXISTS `pacientesactivos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pacientesactivos` (
  `Id_Paciente` bigint(20),
  `nom_Paciente` varchar(50),
  `nom_Estado` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pacientesactivoscafesalud`
--

DROP TABLE IF EXISTS `pacientesactivoscafesalud`;
/*!50001 DROP VIEW IF EXISTS `pacientesactivoscafesalud`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pacientesactivoscafesalud` (
  `Id_Paciente` bigint(20),
  `nom_Paciente` varchar(50),
  `nom_Estado` varchar(50),
  `nom_EPS` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pacientesactivoscapitalsalud`
--

DROP TABLE IF EXISTS `pacientesactivoscapitalsalud`;
/*!50001 DROP VIEW IF EXISTS `pacientesactivoscapitalsalud`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pacientesactivoscapitalsalud` (
  `Id_Paciente` bigint(20),
  `nom_Paciente` varchar(50),
  `nom_Estado` varchar(50),
  `nom_EPS` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pacientesactivoscaprecom`
--

DROP TABLE IF EXISTS `pacientesactivoscaprecom`;
/*!50001 DROP VIEW IF EXISTS `pacientesactivoscaprecom`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pacientesactivoscaprecom` (
  `Id_Paciente` bigint(20),
  `nom_Paciente` varchar(50),
  `nom_Estado` varchar(50),
  `nom_EPS` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pacientesactivossaludtotal`
--

DROP TABLE IF EXISTS `pacientesactivossaludtotal`;
/*!50001 DROP VIEW IF EXISTS `pacientesactivossaludtotal`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pacientesactivossaludtotal` (
  `Id_Paciente` bigint(20),
  `nom_Paciente` varchar(50),
  `nom_Estado` varchar(50),
  `nom_EPS` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pacientesactivossanitas`
--

DROP TABLE IF EXISTS `pacientesactivossanitas`;
/*!50001 DROP VIEW IF EXISTS `pacientesactivossanitas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pacientesactivossanitas` (
  `Id_Paciente` bigint(20),
  `nom_Paciente` varchar(50),
  `nom_Estado` varchar(50),
  `nom_EPS` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tipo_documento`
--

DROP TABLE IF EXISTS `tipo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_documento` (
  `Id_Tipo_Doc` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Tipo_Doc` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Tipo_Doc`),
  UNIQUE KEY `nom_Tipo_Doc` (`nom_Tipo_Doc`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_documento`
--

LOCK TABLES `tipo_documento` WRITE;
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
INSERT INTO `tipo_documento` VALUES (1,'Cédula de Ciudadanía o C.C.'),(3,'Registro de Nacimiento R.N.'),(2,'Tarjeta de Identidad o T.I.');
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_especialidad`
--

DROP TABLE IF EXISTS `tipo_especialidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_especialidad` (
  `Id_Tipo_Espec` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Tipo_Espec` varchar(50) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Tipo_Espec`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_especialidad`
--

LOCK TABLES `tipo_especialidad` WRITE;
/*!40000 ALTER TABLE `tipo_especialidad` DISABLE KEYS */;
INSERT INTO `tipo_especialidad` VALUES (1,'Especialidades clínicas',1),(2,'Especialidades médico-quirúrgicas',1),(3,'Especialidades quirúrgicas',1),(4,'Especialidades de laboratorio o diagnósticas',1);
/*!40000 ALTER TABLE `tipo_especialidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_medicamento`
--

DROP TABLE IF EXISTS `tipo_medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_medicamento` (
  `Id_Tipo_Medicamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Tipo_Medicamento` varchar(50) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Tipo_Medicamento`),
  KEY `estadomedicamento` (`FKId_Estado`),
  CONSTRAINT `tipo_medicamento_ibfk_1` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_medicamento`
--

LOCK TABLES `tipo_medicamento` WRITE;
/*!40000 ALTER TABLE `tipo_medicamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `Id_Tipo_Usuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Tipo_Usuario` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Tipo_Usuario`),
  UNIQUE KEY `nom_Tipo_Usuario` (`nom_Tipo_Usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `Id_Usuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_Usuario` varchar(50) NOT NULL,
  `cont_Usuario` varchar(50) NOT NULL,
  `FKId_Tipo_Usuario` bigint(20) NOT NULL,
  `FKId_Estado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Usuario`),
  KEY `tipousuario` (`FKId_Tipo_Usuario`),
  KEY `estadousuario` (`FKId_Estado`),
  CONSTRAINT `estadousuario` FOREIGN KEY (`FKId_Estado`) REFERENCES `estado` (`Id_Estado`),
  CONSTRAINT `tipousuario` FOREIGN KEY (`FKId_Tipo_Usuario`) REFERENCES `tipo_usuario` (`Id_Tipo_Usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `citasactivasporpaciente`
--

/*!50001 DROP TABLE IF EXISTS `citasactivasporpaciente`*/;
/*!50001 DROP VIEW IF EXISTS `citasactivasporpaciente`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `citasactivasporpaciente` AS select `cita`.`Id_Cita` AS `Id_Cita`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`cita`.`Fecha_Cita` AS `Fecha_Cita`,`estado_cita`.`nom_Estado_Cita` AS `nom_Estado_Cita` from ((`cita` join `estado_cita` on(((`cita`.`FKId_Estado_Cita` = `estado_cita`.`Id_Estado_Cita`) and (`estado_cita`.`nom_Estado_Cita` = 'Pendiente')))) join `paciente` on((`cita`.`FKId_Paciente` = `paciente`.`Id_Paciente`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `citaspendientes`
--

/*!50001 DROP TABLE IF EXISTS `citaspendientes`*/;
/*!50001 DROP VIEW IF EXISTS `citaspendientes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `citaspendientes` AS select `cita`.`Id_Cita` AS `Id_Cita`,`cita`.`Fecha_Cita` AS `Fecha_Cita`,`estado_cita`.`nom_Estado_Cita` AS `nom_Estado_Cita` from (`cita` join `estado_cita` on(((`cita`.`FKId_Estado_Cita` = `estado_cita`.`Id_Estado_Cita`) and (`estado_cita`.`nom_Estado_Cita` = 'Pendiente')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `citaspendientesporpaciente`
--

/*!50001 DROP TABLE IF EXISTS `citaspendientesporpaciente`*/;
/*!50001 DROP VIEW IF EXISTS `citaspendientesporpaciente`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `citaspendientesporpaciente` AS select `cita`.`Id_Cita` AS `Id_Cita`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`cita`.`Fecha_Cita` AS `Fecha_Cita`,`estado_cita`.`nom_Estado_Cita` AS `nom_Estado_Cita` from ((`cita` join `estado_cita` on(((`cita`.`FKId_Estado_Cita` = `estado_cita`.`Id_Estado_Cita`) and (`estado_cita`.`nom_Estado_Cita` = 'Pendiente')))) join `paciente` on((`cita`.`FKId_Paciente` = `paciente`.`Id_Paciente`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `citaspendpacmed`
--

/*!50001 DROP TABLE IF EXISTS `citaspendpacmed`*/;
/*!50001 DROP VIEW IF EXISTS `citaspendpacmed`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `citaspendpacmed` AS select `cita`.`Id_Cita` AS `Id_Cita`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`medico`.`nom_Medico` AS `nom_Medico`,`cita`.`Fecha_Cita` AS `Fecha_Cita`,`estado_cita`.`nom_Estado_Cita` AS `nom_Estado_Cita` from (((`cita` join `estado_cita` on(((`cita`.`FKId_Estado_Cita` = `estado_cita`.`Id_Estado_Cita`) and (`estado_cita`.`nom_Estado_Cita` = 'Pendiente')))) join `paciente` on((`cita`.`FKId_Paciente` = `paciente`.`Id_Paciente`))) join `medico` on((`cita`.`FKId_Medico` = `medico`.`Id_Medico`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `citaspendpacmedoper`
--

/*!50001 DROP TABLE IF EXISTS `citaspendpacmedoper`*/;
/*!50001 DROP VIEW IF EXISTS `citaspendpacmedoper`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `citaspendpacmedoper` AS select `cita`.`Id_Cita` AS `Id_Cita`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`medico`.`nom_Medico` AS `nom_Medico`,`operario`.`nom_Oper` AS `nom_Oper`,`cita`.`Fecha_Cita` AS `Fecha_Cita`,`estado_cita`.`nom_Estado_Cita` AS `nom_Estado_Cita` from ((((`cita` join `estado_cita` on(((`cita`.`FKId_Estado_Cita` = `estado_cita`.`Id_Estado_Cita`) and (`estado_cita`.`nom_Estado_Cita` = 'Pendiente')))) join `paciente` on((`cita`.`FKId_Paciente` = `paciente`.`Id_Paciente`))) join `medico` on((`cita`.`FKId_Medico` = `medico`.`Id_Medico`))) join `operario` on((`cita`.`FKId_Operario` = `operario`.`Id_Operario`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `medicossactivos`
--

/*!50001 DROP TABLE IF EXISTS `medicossactivos`*/;
/*!50001 DROP VIEW IF EXISTS `medicossactivos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `medicossactivos` AS select `medico`.`Id_Medico` AS `Id_Medico`,`medico`.`nom_Medico` AS `nom_Medico`,`estado`.`nom_Estado` AS `nom_Estado` from (`medico` join `estado`) where ((`medico`.`FKId_Estado` = `estado`.`Id_Estado`) and (`estado`.`nom_Estado` = 'Activo')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `operariossactivos`
--

/*!50001 DROP TABLE IF EXISTS `operariossactivos`*/;
/*!50001 DROP VIEW IF EXISTS `operariossactivos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `operariossactivos` AS select `operario`.`Id_Operario` AS `Id_Operario`,`operario`.`nom_Oper` AS `nom_Oper`,`estado`.`nom_Estado` AS `nom_Estado` from (`operario` join `estado`) where ((`operario`.`FKId_Estado` = `estado`.`Id_Estado`) and (`estado`.`nom_Estado` = 'Activo')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pacientesactivos`
--

/*!50001 DROP TABLE IF EXISTS `pacientesactivos`*/;
/*!50001 DROP VIEW IF EXISTS `pacientesactivos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pacientesactivos` AS select `paciente`.`Id_Paciente` AS `Id_Paciente`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`estado`.`nom_Estado` AS `nom_Estado` from (`paciente` join `estado`) where ((`paciente`.`FKId_Estado` = `estado`.`Id_Estado`) and (`estado`.`nom_Estado` = 'Activo')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pacientesactivoscafesalud`
--

/*!50001 DROP TABLE IF EXISTS `pacientesactivoscafesalud`*/;
/*!50001 DROP VIEW IF EXISTS `pacientesactivoscafesalud`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pacientesactivoscafesalud` AS select `paciente`.`Id_Paciente` AS `Id_Paciente`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`estado`.`nom_Estado` AS `nom_Estado`,`eps`.`nom_EPS` AS `nom_EPS` from ((`paciente` join `estado` on(((`paciente`.`FKId_Estado` = `estado`.`Id_Estado`) and (`estado`.`nom_Estado` = 'Activo')))) join `eps` on(((`paciente`.`FKId_EPS` = `eps`.`Id_EPS`) and (`eps`.`nom_EPS` = 'Cafe Salud')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pacientesactivoscapitalsalud`
--

/*!50001 DROP TABLE IF EXISTS `pacientesactivoscapitalsalud`*/;
/*!50001 DROP VIEW IF EXISTS `pacientesactivoscapitalsalud`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pacientesactivoscapitalsalud` AS select `paciente`.`Id_Paciente` AS `Id_Paciente`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`estado`.`nom_Estado` AS `nom_Estado`,`eps`.`nom_EPS` AS `nom_EPS` from ((`paciente` join `estado` on(((`paciente`.`FKId_Estado` = `estado`.`Id_Estado`) and (`estado`.`nom_Estado` = 'Activo')))) join `eps` on(((`paciente`.`FKId_EPS` = `eps`.`Id_EPS`) and (`eps`.`nom_EPS` = 'Capital Salud')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pacientesactivoscaprecom`
--

/*!50001 DROP TABLE IF EXISTS `pacientesactivoscaprecom`*/;
/*!50001 DROP VIEW IF EXISTS `pacientesactivoscaprecom`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pacientesactivoscaprecom` AS select `paciente`.`Id_Paciente` AS `Id_Paciente`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`estado`.`nom_Estado` AS `nom_Estado`,`eps`.`nom_EPS` AS `nom_EPS` from ((`paciente` join `estado` on(((`paciente`.`FKId_Estado` = `estado`.`Id_Estado`) and (`estado`.`nom_Estado` = 'Activo')))) join `eps` on(((`paciente`.`FKId_EPS` = `eps`.`Id_EPS`) and (`eps`.`nom_EPS` = 'Caprecom')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pacientesactivossaludtotal`
--

/*!50001 DROP TABLE IF EXISTS `pacientesactivossaludtotal`*/;
/*!50001 DROP VIEW IF EXISTS `pacientesactivossaludtotal`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pacientesactivossaludtotal` AS select `paciente`.`Id_Paciente` AS `Id_Paciente`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`estado`.`nom_Estado` AS `nom_Estado`,`eps`.`nom_EPS` AS `nom_EPS` from ((`paciente` join `estado` on(((`paciente`.`FKId_Estado` = `estado`.`Id_Estado`) and (`estado`.`nom_Estado` = 'Activo')))) join `eps` on(((`paciente`.`FKId_EPS` = `eps`.`Id_EPS`) and (`eps`.`nom_EPS` = 'Salud Total')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pacientesactivossanitas`
--

/*!50001 DROP TABLE IF EXISTS `pacientesactivossanitas`*/;
/*!50001 DROP VIEW IF EXISTS `pacientesactivossanitas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pacientesactivossanitas` AS select `paciente`.`Id_Paciente` AS `Id_Paciente`,`paciente`.`nom_Paciente` AS `nom_Paciente`,`estado`.`nom_Estado` AS `nom_Estado`,`eps`.`nom_EPS` AS `nom_EPS` from ((`paciente` join `estado` on(((`paciente`.`FKId_Estado` = `estado`.`Id_Estado`) and (`estado`.`nom_Estado` = 'Activo')))) join `eps` on(((`paciente`.`FKId_EPS` = `eps`.`Id_EPS`) and (`eps`.`nom_EPS` = 'Sanitas')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-19 20:10:12
